/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __GTK_VTREE_H__
#define __GTK_VTREE_H__


#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_VTREE			(gtk_vtree_get_type ())
#define GTK_VTREE(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_VTREE, GtkVTree))
#define GTK_VTREE_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_VTREE, GtkVTreeClass))
#define GTK_IS_VTREE(obj)		(GTK_CHECK_TYPE ((obj), GTK_TYPE_VTREE))
#define GTK_IS_VTREE_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_VTREE))

typedef struct _GtkVTree       GtkVTree;
typedef struct _GtkVTreeClass  GtkVTreeClass;



typedef enum 
{
  GTK_VTREE_NOCHILDREN, /* Can't be expanded/collapsed */
  GTK_VTREE_EXPANDED,   /* Is expanded */
  GTK_VTREE_COLLAPSED   /* Is collapsed */
} GtkVTreeNodeState;

typedef struct _GtkVTreeNodeInfo GtkVTreeNodeInfo;

struct _GtkVTreeNodeInfo 
{
  /* Is this node expanded, collapsed, or has no children anyway
   */

  GtkVTreeNodeState state;

};

/* How to render each cell */

typedef enum 
{
  GTK_VTREE_CELL_BLANK,   /* Blank cell */
  GTK_VTREE_CELL_PIXTEXT, /* Pixmap and/or text, like CTree */
  GTK_VTREE_CELL_TOGGLE,  /* Toggle button */
  GTK_VTREE_CELL_RADIO,   /* Radio button */
  GTK_VTREE_CELL_USER     /* Custom cell drawn by user code */
  /*  GTK_VTREE_CELL_ENTRY,*/   /* Editable text - disabled pending GtkEntry backend abstraction */

} GtkVTreeCellType;


/* To use the GtkVTree, you must create a GtkVTreeTable. Basically
 *  this is a set of functions to be used to access your tree
 *  data. (The purpose of GtkVTree is to avoid data copies and
 *  increase flexibility - it is thus more annoying to use than
 *  GtkCTree.) The "gpointer node" arguments and gpointer return 
 *  values represent a data type provided by you and opaque to 
 *  GtkVTree, which may well be GNode but does not have to be.
 */

typedef struct _GtkVTreeVTable GtkVTreeVTable;

struct _GtkVTreeVTable 
{
  /* Should fill in GtkVTreeNodeInfo with info for node; must be
   * constant-time fast 
   */
  
  void (* vtree_node_info) (GtkVTree* vtree,
                            gpointer node, GtkVTreeNodeInfo* info);

  /* GtkVTree will use these tree-walking functions to traverse and
   * render the tree. It will traverse all nodes which are "visible,"
   * but little more; this means each node whose
   * parent nodes are expanded. Of course the first node has no 
   * parent. Your tree must be traversable up/down and left/right,
   * i.e. doubly linked in two dimensions.
   */

  /* Fetch the "top left" node, i.e. the first node in the top level
   * of the tree. Your tree is not required to have a single root.  
   * NULL return means the tree is empty.
   */

  gpointer (* vtree_node_first)       (GtkVTree* vtree);

  /* Start of list of this node's children, NULL if none. Does not
     have to be implemented; if it isn't, the tree will not be
     recursed (i.e. you will just get a list.) */
  gpointer (* vtree_node_first_child) (GtkVTree* vtree, gpointer node);
  
  gpointer (* vtree_node_parent)      (GtkVTree* vtree, gpointer node);

  /* Next sibling of node, NULL if none */
  gpointer (* vtree_node_next)        (GtkVTree* vtree, gpointer node);
  /* Previous sibling, NULL if none */
  gpointer (* vtree_node_prev)        (GtkVTree* vtree, gpointer node);

  /* This is used for drag and drop; can be NULL if you disable
   * drag and drop tree manipulation. It moves node before 
   * sibling.
   */
  
  gpointer (* vtree_node_insert_before)(GtkVTree* vtree, 
                                        gpointer sibling, 
                                        gpointer node);

  /* These two functions are optional; if they are non-NULL, the VTree
   * uses them to find the current size of the tree and get the
   * on-screen nodes instead of using the above functions (though the
   * above are still required).  Basically this permits you to use an
   * array instead of a tree structure for efficiency, if your data is
   * really a list and not a tree. You can still use them with a tree,
   * say if you are maintaining this info anyway. If they are not constant
   * time operations, do NOT implement them!
   */

  /* Return number of rows in the tree's current state, considering
   * expanded/contracted state of each node. 
   */
  guint    (* vtree_n_rows)           (GtkVTree* vtree); 

  /* Return visible node number n, or NULL if none */
  gpointer (* vtree_nth_node)         (GtkVTree* vtree, guint n);

  /* Fill array of cell types; will be called only if the node is 
   * being displayed. Array size matches number of tree columns.
   */
  void     (* vtree_node_celltypes)   (GtkVTree* vtree, 
                                       gpointer node, 
                                       GtkVTreeCellType* fillme);
  
  /* Any of the following can be NULL if you don't have cells 
   * of the matching type 
   */

  /* Called for radio and toggle cells */
  gboolean (* vtree_node_get_state)   (GtkVTree* vtree,
                                       gpointer node, guint column);
  void     (* vtree_node_set_state)   (GtkVTree* vtree,
                                       gpointer node, guint column, 
                                       gboolean state);

  /* Called for pixtext cells */
  void     (* vtree_node_get_pixtext) (GtkVTree* vtree,
                                       gpointer node, guint column,
                                       GdkPixmap** pixmap, 
                                       GdkBitmap** mask,
                                       const gchar** text);

  /* Called for user-drawn cells */
  void     (* vtree_node_draw)        (GtkVTree* vtree,
                                       gpointer node, 
                                       guint column,
                                       GtkStateType state,
                                       GdkDrawable* drawable, 
                                       GdkRectangle* rect);

  /* These can be NULL if you don't have a tree column.
   * The user must maintain expanded/collapsed state of 
   * tree nodes.
   */
  void     (* vtree_node_expand)      (GtkVTree* vtree,
                                       gpointer node);

  void     (* vtree_node_collapse)    (GtkVTree* vtree,
                                       gpointer node);
  
  /* The user also has to maintain the current selection. 
   * Otherwise the tree would have to do some fairly slow 
   * stuff to verify the selection still exists when the tree 
   * structure changed.
   */
  
  /* Return allocated list of selected nodes */
  GSList*  (* vtree_get_selection)    (GtkVTree* vtree);

  gboolean (* vtree_node_selected)    (GtkVTree* vtree,
                                       gpointer node);

  /* Add node to selection */
  void     (* vtree_node_select)      (GtkVTree* vtree,
                                       gpointer node);

  /* Remove node from selection */
  void     (* vtree_node_unselect)    (GtkVTree* vtree,
                                       gpointer node);
  
  /* And the user has to maintain the focused row... */

  gpointer (* vtree_get_focus_node)   (GtkVTree* vtree);

  void     (* vtree_node_focus_in)    (GtkVTree* vtree,
                                       gpointer node);

  void     (* vtree_node_focus_out)   (GtkVTree* vtree,
                                       gpointer node);
};



/* All GtkVTree members are private, and will be changed whenever 
 * we feel like it. So don't touch.
 */

struct _GtkVTree
{
  GtkWidget widget;

  /* On the next redraw, the structure must be re-scanned */
  guint16 structure_changed : 1;
  /* Titles aren't shown */
  guint16 titles_hidden     : 1;

  GtkVTreeVTable* vtable;

  /* We cache the following data about the tree. It must be re-assessed
   * whenever the tree structure changes.
   */
  guint total_rows;         /* Count of rows in the entire tree,
                             * considering current expand/collapse
                             * states. 
                             */
  GPtrArray* onscreen_nodes; /* Array of nodes which are on-screen. The
                              * array contains an internal structure
                              * with info about the node. 
                              */
  gpointer last_node;        /* Last node in the tree */

  /* End of cached data */

  gint level_indent; /* Amount to indent with each tree level */
  gint row_spacing;  /* Space between each row in the tree */
  gint row_height;   /* How high each row is. */
  gint tree_column;  /* Which column contains the tree expanders, 
                      * -1 for none 
                      */

  guint columns;           /* Number of columns */
  gint title_height;       /* How high the column titles are. */
  gint frame_width;        /* How much space to leave for the frame */
  gint column_spacing;     /* spacing between columns */
  gint* column_widths;     /* array of column widths */
  gint* column_xpositions; /* column X positions */
  gchar** titles;          /* column titles */
  
  /* Sum of all column widths + intercolumn spacing */
  gint entire_list_width;

  /* Width of the visible list area in pixels (not counting frame) */
  gint list_width;

  /* Height of the visible list area (pixels) */
  gint list_height;

  /* Scroll adjustments. The adjustment value represents the first
   * visible pixel of the list, counting from 0 for the top of row 0
   * (vertical) or 0 for the left of column 0 (horizontal) 
   */

  GtkAdjustment *hadjustment;
  GtkAdjustment *vadjustment;
  
  /* Scroll offset; number of pixels from the top left of the first
   * row. 
   */

  gint voffset;
  gint hoffset;

  /* Style to use for the frame */
  GtkShadowType shadow_type;

  /* Selection mode */
  GtkSelectionMode selection_mode;

  /* Random X resources and display cruft */
  GdkGC* xor_gc;
};

struct _GtkVTreeClass
{
  GtkWidgetClass        parent_class;
  
  /* For the GtkScrolledWindow interface */
  void  (*set_scroll_adjustments) (GtkVTree       *vtree,
				   GtkAdjustment  *hadjustment,
				   GtkAdjustment  *vadjustment);

  void   (*select_node)          (GtkVTree       *vtree,
                                  gpointer        node,
                                  GdkEvent       *event);
  void   (*unselect_node)        (GtkVTree       *vtree,
                                  gpointer        node,
                                  GdkEvent       *event);
  void   (*row_move)            (GtkVTree       *vtree,
				 gint            source_row,
				 gint            dest_row);
  void   (*click_column)        (GtkVTree       *vtree,
				 gint            column);
  void   (*resize_column)       (GtkVTree       *vtree,
				 gint            column,
                                 gint            width);
  void   (*toggle_focus_row)    (GtkVTree       *vtree);
  void   (*select_all)          (GtkVTree       *vtree);
  void   (*unselect_all)        (GtkVTree       *vtree);
  void   (*undo_selection)      (GtkVTree       *vtree);
  void   (*start_selection)     (GtkVTree       *vtree);
  void   (*end_selection)       (GtkVTree       *vtree);
  void   (*extend_selection)    (GtkVTree       *vtree,
				 GtkScrollType   scroll_type,
				 gfloat          position,
				 gboolean        auto_start_selection);
  void   (*scroll_horizontal)   (GtkVTree       *vtree,
				 GtkScrollType   scroll_type,
				 gfloat          position);
  void   (*scroll_vertical)     (GtkVTree       *vtree,
				 GtkScrollType   scroll_type,
				 gfloat          position);
  void   (*toggle_add_mode)     (GtkVTree       *vtree);
  void   (*abort_column_resize) (GtkVTree       *vtree);
};



GtkType        gtk_vtree_get_type       (void);

/* Defaults to hidden titles */
GtkWidget*     gtk_vtree_new            (GtkVTreeVTable* table, 
                                         guint ncols);

/* Defaults to visible titles */
GtkWidget*     gtk_vtree_new_with_titles(GtkVTreeVTable* table,
                                         guint ncols, 
                                         const gchar* titles[]);

/* Set number of columns; if titles is NULL, then titles are not
 * shown. If non-NULL, they are shown.
 */
void           gtk_vtree_set_columns    (GtkVTree* vtree,
                                         guint ncols,
                                         const gchar* titles[]);

/* Set which column should contain the expanders; by default, no 
 * column does, and the user can't expand/contract rows.
 * Set to -1 for no expanders.
 */
void           gtk_vtree_set_tree_column(GtkVTree* vtree, gint column);


/* The following two functions queue a one-shot idle function
 * rather than acting immediately.
 */

/* Redisplay the rows currently on-screen, since their content
 * has changed.
 */
void           gtk_vtree_data_changed   (GtkVTree* vtree);

/* Rescan the tree and redisplay, since nodes have been added or
 * removed or expanded/collapsed. Node pointers are marked invalid and
 * will not be reused by the VTree after this function is called,
 * unless they are re-obtained from the vtable functions.  
 * Implies gtk_vtree_data_changed() behavior
 */
void           gtk_vtree_structure_changed (GtkVTree* vtree);


/* Scroll adjustments; usually GtkScrolledWindow will handle this. */
void           gtk_vtree_set_hadjustment (GtkVTree* vtree, 
                                          GtkAdjustment* adjustment);
GtkAdjustment* gtk_vtree_get_hadjustment (GtkVTree* vtree);
void           gtk_vtree_set_vadjustment (GtkVTree* vtree, 
                                          GtkAdjustment* adjustment);
GtkAdjustment* gtk_vtree_get_vadjustment (GtkVTree* vtree);


/* Will eventually have most of the CTree functionality, when
 * applicable.  Row "styles" won't be available, since the user can
 * always draw cells themselves. Probably also won't fool with 
 * line and expander styles.
 */

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_VTREE_H__ */
