/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include "gtkvtree.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkbindings.h>

/* Macros to use the vtable */

#define NODE_INFO(vtree, node, info) ((*vtree->vtable->vtree_node_info)(vtree,node,info))

#define NODE_FIRST(vtree)            ((*vtree->vtable->vtree_node_first)(vtree))

#define NODE_FIRST_CHILD(vtree, node) ((*vtree->vtable->vtree_node_first_child)(vtree,node))

#define NODE_NEXT(vtree, node)  ((*vtree->vtable->vtree_node_next)(vtree,node))
#define NODE_PREV(vtree, node)  ((*vtree->vtable->vtree_node_prev)(vtree,node))

#define N_ROWS(vtree)           ((*vtree->vtable->vtree_n_rows)(vtree))

#define NTH_NODE(vtree, nth)    ((*vtree->vtable->vtree_nth_node)(vtree, nth))

#define NODE_CELLTYPES(vtree, node, fillme) ((*vtree->vtable->vtree_node_celltypes)(vtree, node, fillme))

#define NODE_GET_PIXTEXT(vtree, node, column, pixmap, mask, text) ((*vtree->vtable->vtree_node_get_pixtext)(vtree, node, column, pixmap, mask, text))

#define GET_FOCUS_NODE(vtree) ((*vtree->vtable->vtree_get_focus_node)(vtree))
#define NODE_FOCUS_IN(vtree, node) ((*vtree->vtable->vtree_node_focus_in)(vtree,node))
#define NODE_FOCUS_OUT(vtree, node) ((*vtree->vtable->vtree_node_focus_out)(vtree,node))



/* Other macros */
#define TITLE_HEIGHT(vtree)  (vtree->titles_hidden ? 0 : vtree->title_height)

#define ONSCREEN_ROWS_HEIGHT(vtree)  (GTK_WIDGET(vtree)->allocation.height - TITLE_HEIGHT(vtree) - vtree->frame_width*2)

#define N_ROWS_ONSCREEN(vtree) (MAX(0, (ONSCREEN_ROWS_HEIGHT(vtree)/(vtree->row_height+vtree->row_spacing) + 1)))

#define FIRST_ROW_INDEX(vtree) (((NodeInfo*)vtree->onscreen_nodes[0])->row)

#define ENTIRE_LIST_HEIGHT(vtree) (vtree->total_rows*(vtree->row_height+vtree->row_spacing))

#define ROW_OFFSET(vtree, row) (row*(vtree->row_height+vtree->row_spacing))

#define ROW_AT_OFFSET(vtree, offset) (offset/(vtree->row_height+vtree->row_spacing))

#define ROW_TOPPIXEL(vtree, row) ((ROW_OFFSET(vtree, row) - vtree->voffset) \
                                  + TITLE_HEIGHT(vtree) + vtree->frame_width)

#define ROW_AT_PIXEL(vtree, pixel) ((TITLE_HEIGHT(vtree) + vtree->frame_width \
                                     + vtree->voffset)/(vtree->row_height+vtree->row_spacing))

/* Information stored about nodes which are on-screen. */
typedef struct _NodeInfo NodeInfo;

struct _NodeInfo {
  GtkVTreeNodeInfo public_info; /* From the vtable function */
  gpointer node;   /* The node from the VTable */
  guint depth;     /* How far into the tree we are */
  guint row;       /* Row index, in [0, vtree->total_rows) */
};

enum {
  ARG_0
  /* FIXME */
};

static void gtk_vtree_class_init          (GtkVTreeClass     *klass);
static void gtk_vtree_init                (GtkVTree          *vtree);
static void gtk_vtree_set_arg      	  (GtkObject         *object,
					   GtkArg            *arg,
					   guint              arg_id);
static void gtk_vtree_get_arg		  (GtkObject         *object,
					   GtkArg            *arg,
					   guint              arg_id);
static void gtk_vtree_finalize            (GtkObject         *object);
static void gtk_vtree_destroy             (GtkObject         *object);
static void gtk_vtree_realize             (GtkWidget         *widget);
static void gtk_vtree_unrealize           (GtkWidget         *widget);
static void gtk_vtree_size_request        (GtkWidget         *widget,
					   GtkRequisition    *requisition);
static void gtk_vtree_size_allocate       (GtkWidget         *widget,
					   GtkAllocation     *allocation);
static void gtk_vtree_draw                (GtkWidget         *widget,
					   GdkRectangle      *area);
static gint gtk_vtree_expose              (GtkWidget         *widget,
					   GdkEventExpose    *event);
static gint gtk_vtree_button_press        (GtkWidget         *widget,
					   GdkEventButton    *event);
static gint gtk_vtree_button_release      (GtkWidget         *widget,
					   GdkEventButton    *event);
static gint gtk_vtree_motion_notify       (GtkWidget         *widget,
					   GdkEventMotion    *event);
static gint gtk_vtree_key_press           (GtkWidget         *widget,
					   GdkEventKey       *event);
static gint gtk_vtree_focus_in            (GtkWidget         *widget,
					   GdkEventFocus     *event);
static gint gtk_vtree_focus_out           (GtkWidget         *widget,
					   GdkEventFocus     *event);
static void gtk_vtree_style_set	          (GtkWidget         *widget,
					   GtkStyle          *previous_style);
static void gtk_vtree_state_changed	  (GtkWidget         *widget,
					   GtkStateType       previous_state);


static void gtk_vtree_xor_focus           (GtkVTree* vtree);

static void gtk_vtree_set_scroll_adjustments (GtkVTree      *vtree,
                                              GtkAdjustment *hadjustment,
                                              GtkAdjustment *vadjustment);

/* Bindings */
static void   gtk_vtree_select_node         (GtkVTree       *vtree,
                                             gpointer        node,
                                             GdkEvent       *event);
static void   gtk_vtree_unselect_node       (GtkVTree       *vtree,
                                             gpointer        node,
                                             GdkEvent       *event);
static void   gtk_vtree_row_move            (GtkVTree       *vtree,
                                             gint            source_row,
                                             gint            dest_row);
static void   gtk_vtree_click_column        (GtkVTree       *vtree,
                                             gint            column);
static void   gtk_vtree_resize_column       (GtkVTree       *vtree,
                                             gint            column,
                                             gint            width);
static void   gtk_vtree_toggle_focus_row    (GtkVTree       *vtree);
static void   gtk_vtree_select_all          (GtkVTree       *vtree);
static void   gtk_vtree_unselect_all        (GtkVTree       *vtree);
static void   gtk_vtree_undo_selection      (GtkVTree       *vtree);
static void   gtk_vtree_start_selection     (GtkVTree       *vtree);
static void   gtk_vtree_end_selection       (GtkVTree       *vtree);
static void   gtk_vtree_extend_selection    (GtkVTree       *vtree,
                                             GtkScrollType   scroll_type,
                                             gfloat          position,
                                             gboolean        auto_start_selection);
static void   gtk_vtree_scroll_horizontal   (GtkVTree       *vtree,
                                             GtkScrollType   scroll_type,
                                             gfloat          position);
static void   gtk_vtree_scroll_vertical     (GtkVTree       *vtree,
                                             GtkScrollType   scroll_type,
                                             gfloat          position);
static void   gtk_vtree_toggle_add_mode     (GtkVTree       *vtree);
static void   gtk_vtree_abort_column_resize (GtkVTree       *vtree);

static void   gtk_vtree_moveto_percent(GtkVTree* vtree, 
                                       gfloat xpercent,
                                       gfloat ypercent);

/* Cheesy internal functions */

static void setup_hadjustment (GtkVTree *vtree);
static void setup_vadjustment (GtkVTree *vtree);
/* Next *row*, not next node on same tree level */
static gpointer next_row_node (GtkVTree* vtree, gpointer cur);
static gpointer prev_row_node (GtkVTree* vtree, gpointer cur);
/* Row number - may not be O(1) */
static gpointer scan_to_row (GtkVTree* vtree, guint row);
/* Move the focus from old to new */
static void change_focus(GtkVTree* vtree, gpointer old, gpointer new);
/* Get column at window pixel */
static gint column_at_xpixel(GtkVTree* vtree, gint xpixel);
static gint closest_column_at_xpixel(GtkVTree* vtree, gint xpixel);
static gboolean node_is_onscreen(GtkVTree* vtree, gpointer node);

/* Signals */
enum
{
  SELECT_NODE,
  UNSELECT_NODE,
  ROW_MOVE,
  CLICK_COLUMN,
  RESIZE_COLUMN,
  TOGGLE_FOCUS_ROW,
  SELECT_ALL,
  UNSELECT_ALL,
  UNDO_SELECTION,
  START_SELECTION,
  END_SELECTION,
  TOGGLE_ADD_MODE,
  EXTEND_SELECTION,
  SCROLL_VERTICAL,
  SCROLL_HORIZONTAL,
  ABORT_COLUMN_RESIZE,
  LAST_SIGNAL
};

static GtkWidgetClass *parent_class = NULL;

static guint vtree_signals[LAST_SIGNAL] = {0};

GtkType
gtk_vtree_get_type (void)
{
  static GtkType vtree_type = 0;

  if (!vtree_type)
    {
      static const GtkTypeInfo vtree_info =
      {
	"GtkVTree",
	sizeof (GtkVTree),
	sizeof (GtkVTreeClass),
	(GtkClassInitFunc) gtk_vtree_class_init,
	(GtkObjectInitFunc) gtk_vtree_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      vtree_type = gtk_type_unique (GTK_TYPE_WIDGET, &vtree_info);
    }

  return vtree_type;
}

static void
gtk_vtree_class_init (GtkVTreeClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkBindingSet *binding_set;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class (GTK_TYPE_WIDGET);

  widget_class->set_scroll_adjustments_signal =
    gtk_signal_new ("set_scroll_adjustments",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, set_scroll_adjustments),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2, 
                    GTK_TYPE_ADJUSTMENT, GTK_TYPE_ADJUSTMENT);

  vtree_signals[SELECT_NODE] =
    gtk_signal_new ("select_node",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, select_node),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_GDK_EVENT);
  vtree_signals[UNSELECT_NODE] =
    gtk_signal_new ("unselect_node",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, unselect_node),
		    gtk_marshal_NONE__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER, 
                    GTK_TYPE_GDK_EVENT);
  vtree_signals[ROW_MOVE] =
    gtk_signal_new ("row_move",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, row_move),
		    gtk_marshal_NONE__INT_INT,
		    GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_INT);
  vtree_signals[CLICK_COLUMN] =
    gtk_signal_new ("click_column",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, click_column),
		    gtk_marshal_NONE__INT,
		    GTK_TYPE_NONE, 1, GTK_TYPE_INT);
  vtree_signals[RESIZE_COLUMN] =
    gtk_signal_new ("resize_column",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, resize_column),
		    gtk_marshal_NONE__INT_INT,
		    GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_INT);

  vtree_signals[TOGGLE_FOCUS_ROW] =
    gtk_signal_new ("toggle_focus_row",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, toggle_focus_row),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);
  vtree_signals[SELECT_ALL] =
    gtk_signal_new ("select_all",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, select_all),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);
  vtree_signals[UNSELECT_ALL] =
    gtk_signal_new ("unselect_all",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, unselect_all),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);
  vtree_signals[UNDO_SELECTION] =
    gtk_signal_new ("undo_selection",
		    GTK_RUN_LAST | GTK_RUN_ACTION,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, undo_selection),
		    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0);
  vtree_signals[START_SELECTION] =
    gtk_signal_new ("start_selection",
		    GTK_RUN_LAST | GTK_RUN_ACTION,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, start_selection),
		    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0);

  vtree_signals[END_SELECTION] =
    gtk_signal_new ("end_selection",
		    GTK_RUN_LAST | GTK_RUN_ACTION,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, end_selection),
		    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0);

  vtree_signals[TOGGLE_ADD_MODE] =
    gtk_signal_new ("toggle_add_mode",
		    GTK_RUN_LAST | GTK_RUN_ACTION,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkVTreeClass, toggle_add_mode),
		    gtk_marshal_NONE__NONE,
		    GTK_TYPE_NONE, 0);
  vtree_signals[EXTEND_SELECTION] =
    gtk_signal_new ("extend_selection",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, extend_selection),
                    gtk_marshal_NONE__ENUM_FLOAT_BOOL,
                    GTK_TYPE_NONE, 3,
		    GTK_TYPE_SCROLL_TYPE, GTK_TYPE_FLOAT, GTK_TYPE_BOOL);
  vtree_signals[SCROLL_VERTICAL] =
    gtk_signal_new ("scroll_vertical",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, scroll_vertical),
                    gtk_marshal_NONE__ENUM_FLOAT,
                    GTK_TYPE_NONE, 2, GTK_TYPE_SCROLL_TYPE, GTK_TYPE_FLOAT);
  vtree_signals[SCROLL_HORIZONTAL] =
    gtk_signal_new ("scroll_horizontal",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, scroll_horizontal),
                    gtk_marshal_NONE__ENUM_FLOAT,
                    GTK_TYPE_NONE, 2, GTK_TYPE_SCROLL_TYPE, GTK_TYPE_FLOAT);

  vtree_signals[ABORT_COLUMN_RESIZE] =
    gtk_signal_new ("abort_column_resize",
                    GTK_RUN_LAST | GTK_RUN_ACTION,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkVTreeClass, abort_column_resize),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);

  object_class->set_arg = gtk_vtree_set_arg;
  object_class->get_arg = gtk_vtree_get_arg;
  object_class->finalize = gtk_vtree_finalize;
  object_class->destroy = gtk_vtree_destroy;

  widget_class->realize = gtk_vtree_realize;
  widget_class->unrealize = gtk_vtree_unrealize;
  /*  widget_class->draw_focus = gtk_vtree_draw_focus; */
  widget_class->size_request = gtk_vtree_size_request;
  widget_class->size_allocate = gtk_vtree_size_allocate;
  widget_class->draw = gtk_vtree_draw;
  widget_class->expose_event = gtk_vtree_expose;
  widget_class->button_press_event = gtk_vtree_button_press;
  widget_class->button_release_event = gtk_vtree_button_release;
  widget_class->motion_notify_event = gtk_vtree_motion_notify;
  widget_class->key_press_event = gtk_vtree_key_press;
  widget_class->focus_in_event = gtk_vtree_focus_in;
  widget_class->focus_out_event = gtk_vtree_focus_out;
  widget_class->style_set = gtk_vtree_style_set;
  widget_class->state_changed = gtk_vtree_state_changed;

  class->set_scroll_adjustments = gtk_vtree_set_scroll_adjustments;

  class->select_node = gtk_vtree_select_node;
  class->unselect_node = gtk_vtree_unselect_node;
  class->row_move = gtk_vtree_row_move;
  class->click_column = gtk_vtree_click_column;
  class->resize_column = gtk_vtree_resize_column;
  class->toggle_focus_row = gtk_vtree_toggle_focus_row;
  class->select_all = gtk_vtree_select_all;
  class->unselect_all = gtk_vtree_unselect_all;
  class->undo_selection = gtk_vtree_undo_selection;
  class->start_selection = gtk_vtree_start_selection;
  class->end_selection = gtk_vtree_end_selection;
  class->extend_selection = gtk_vtree_extend_selection;
  class->scroll_horizontal = gtk_vtree_scroll_horizontal;
  class->scroll_vertical = gtk_vtree_scroll_vertical;
  class->toggle_add_mode = gtk_vtree_toggle_add_mode;
  class->abort_column_resize = gtk_vtree_abort_column_resize;

  binding_set = gtk_binding_set_by_class (class);

  gtk_binding_entry_add_signal (binding_set, GDK_Up, 0,
				"scroll_vertical", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_STEP_BACKWARD,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Down, 0,
				"scroll_vertical", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_STEP_FORWARD,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Page_Up, 0,
				"scroll_vertical", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_PAGE_BACKWARD,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Page_Down, 0,
				"scroll_vertical", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_PAGE_FORWARD,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Home, GDK_CONTROL_MASK,
				"scroll_vertical", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_JUMP,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_End, GDK_CONTROL_MASK,
				"scroll_vertical", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_JUMP,
				GTK_TYPE_FLOAT, 1.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Up, GDK_SHIFT_MASK,
				"extend_selection", 3,
				GTK_TYPE_ENUM, GTK_SCROLL_STEP_BACKWARD,
				GTK_TYPE_FLOAT, 0.0, GTK_TYPE_BOOL, TRUE);
  gtk_binding_entry_add_signal (binding_set, GDK_Down, GDK_SHIFT_MASK,
				"extend_selection", 3,
				GTK_TYPE_ENUM, GTK_SCROLL_STEP_FORWARD,
				GTK_TYPE_FLOAT, 0.0, GTK_TYPE_BOOL, TRUE);
  gtk_binding_entry_add_signal (binding_set, GDK_Page_Up, GDK_SHIFT_MASK,
				"extend_selection", 3,
				GTK_TYPE_ENUM, GTK_SCROLL_PAGE_BACKWARD,
				GTK_TYPE_FLOAT, 0.0, GTK_TYPE_BOOL, TRUE);
  gtk_binding_entry_add_signal (binding_set, GDK_Page_Down, GDK_SHIFT_MASK,
				"extend_selection", 3,
				GTK_TYPE_ENUM, GTK_SCROLL_PAGE_FORWARD,
				GTK_TYPE_FLOAT, 0.0, GTK_TYPE_BOOL, TRUE);
  gtk_binding_entry_add_signal (binding_set, GDK_Home,
				GDK_SHIFT_MASK | GDK_CONTROL_MASK,
				"extend_selection", 3,
				GTK_TYPE_ENUM, GTK_SCROLL_JUMP,
				GTK_TYPE_FLOAT, 0.0, GTK_TYPE_BOOL, TRUE);
  gtk_binding_entry_add_signal (binding_set, GDK_End,
				GDK_SHIFT_MASK | GDK_CONTROL_MASK,
				"extend_selection", 3,
				GTK_TYPE_ENUM, GTK_SCROLL_JUMP,
				GTK_TYPE_FLOAT, 1.0, GTK_TYPE_BOOL, TRUE);
  gtk_binding_entry_add_signal (binding_set, GDK_Left, 0,
				"scroll_horizontal", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_STEP_BACKWARD,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Right, 0,
				"scroll_horizontal", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_STEP_FORWARD,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Home, 0,
				"scroll_horizontal", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_JUMP,
				GTK_TYPE_FLOAT, 0.0);
  gtk_binding_entry_add_signal (binding_set, GDK_End, 0,
				"scroll_horizontal", 2,
				GTK_TYPE_ENUM, GTK_SCROLL_JUMP,
				GTK_TYPE_FLOAT, 1.0);
  gtk_binding_entry_add_signal (binding_set, GDK_Escape, 0,
				"undo_selection", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_Escape, 0,
				"abort_column_resize", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_space, 0,
				"toggle_focus_row", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_space, GDK_CONTROL_MASK,
				"toggle_add_mode", 0);
  gtk_binding_entry_add_signal (binding_set, '/', GDK_CONTROL_MASK,
				"select_all", 0);
  gtk_binding_entry_add_signal (binding_set, '\\', GDK_CONTROL_MASK,
				"unselect_all", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_Shift_L,
				GDK_RELEASE_MASK | GDK_SHIFT_MASK,
				"end_selection", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_Shift_R,
				GDK_RELEASE_MASK | GDK_SHIFT_MASK,
				"end_selection", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_Shift_L,
				GDK_RELEASE_MASK | GDK_SHIFT_MASK |
				GDK_CONTROL_MASK,
				"end_selection", 0);
  gtk_binding_entry_add_signal (binding_set, GDK_Shift_R,
				GDK_RELEASE_MASK | GDK_SHIFT_MASK |
				GDK_CONTROL_MASK,
				"end_selection", 0);
}

static void
gtk_vtree_set_arg (GtkObject      *object,
		   GtkArg         *arg,
		   guint           arg_id)
{
  GtkVTree* vtree;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_VTREE(object));

  vtree = GTK_VTREE (object);

  switch (arg_id)
    {

    default:
      break;
    }
}

static void
gtk_vtree_get_arg (GtkObject      *object,
		   GtkArg         *arg,
		   guint           arg_id)
{
  GtkVTree* vtree;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_VTREE(object));

  vtree = GTK_VTREE (object);

  switch (arg_id)
    {

    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

static void
gtk_vtree_init (GtkVTree* vtree)
{
  GTK_WIDGET_SET_FLAGS (vtree, GTK_CAN_FOCUS);

  vtree->structure_changed = TRUE;
  vtree->titles_hidden     = TRUE;
  vtree->vtable            = NULL;
  vtree->total_rows        = 0;
  vtree->onscreen_nodes    = NULL;
  vtree->last_node         = NULL;
  vtree->level_indent      = 15;
  vtree->row_spacing       = 3;
  vtree->row_height        = 20;
  vtree->tree_column       = -1;
  vtree->columns           = 0;
  vtree->title_height      = 25;
  vtree->frame_width       = 10; /* For debugging, I want it big */
  vtree->column_spacing    = 5;  /* ditto */
  vtree->titles            = NULL;
  vtree->column_widths     = NULL;
  vtree->column_xpositions = NULL;
  vtree->entire_list_width = 0;
  vtree->list_width        = 0;
  vtree->list_height       = 0;
  vtree->hadjustment       = NULL;
  vtree->vadjustment       = NULL;
  vtree->voffset           = 0;
  vtree->hoffset           = 0;
  vtree->shadow_type       = GTK_SHADOW_IN;
  vtree->selection_mode    = GTK_SELECTION_SINGLE;
  vtree->xor_gc            = NULL;
  
}

GtkWidget*     
gtk_vtree_new (GtkVTreeVTable* table, 
               guint ncols)
{
  GtkWidget* widget = GTK_WIDGET (gtk_type_new (GTK_TYPE_VTREE));
  GtkVTree*  vtree = GTK_VTREE(widget);

  vtree->vtable = table;

  vtree->titles_hidden = TRUE;

  gtk_vtree_set_columns(vtree, ncols, NULL);

  return widget;
}

GtkWidget*     
gtk_vtree_new_with_titles (GtkVTreeVTable* table,
                           guint ncols, 
                           const gchar* titles[])
{
  GtkWidget* widget = GTK_WIDGET (gtk_type_new (GTK_TYPE_VTREE));
  GtkVTree*  vtree = GTK_VTREE(widget);

  vtree->vtable = table;

  vtree->titles_hidden = FALSE;

  gtk_vtree_set_columns(vtree, ncols, titles);
  
  return widget;
}

static void
free_titles(GtkVTree* vtree)
{
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  if (vtree->titles == NULL) 
    return;

  g_strfreev(vtree->titles);
  vtree->titles = NULL;
}

static void
update_list_width(GtkVTree* vtree)
{
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));
  g_return_if_fail(vtree->columns >= 1);

  if (vtree->columns == 1)
    {
      vtree->entire_list_width = vtree->column_widths[0] + vtree->column_spacing*2;
    }
  else
    {
      vtree->entire_list_width = 
        vtree->column_xpositions[vtree->columns-1] - vtree->column_xpositions[0] + 
        vtree->column_widths[vtree->columns-1] + vtree->column_spacing*2;
    }

  vtree->list_width = MAX(0, (GTK_WIDGET(vtree)->allocation.width - vtree->frame_width*2));

  /* Update hadjustment */
  setup_hadjustment(vtree);
}

void           
gtk_vtree_set_columns    (GtkVTree* vtree,
                          guint ncols,
                          const gchar* titles[])
{
  g_return_if_fail(ncols > 0);
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  vtree->columns = ncols;

  if (vtree->titles != NULL)
    free_titles(vtree);

  if (vtree->column_widths != NULL)
    g_free(vtree->column_widths);

  if (vtree->column_xpositions != NULL)
    g_free(vtree->column_xpositions);

  vtree->column_widths = g_new0(gint, ncols);
  vtree->column_xpositions = g_new0(gint, ncols);

  {
    guint i = 0;
    gint xpos = vtree->frame_width + vtree->column_spacing;
    while (i < ncols)
      {
        /* FIXME don't hardcode default width */
        vtree->column_widths[i] = 100;
        vtree->column_xpositions[i] = xpos;

        xpos += vtree->column_widths[i] + vtree->column_spacing;

        ++i;
      }
  }

  update_list_width(vtree);

  if (titles != NULL)
    {
      guint i;

      vtree->titles = g_new0(gchar*, ncols+1);

      i = 0;
      while (i < ncols)
        {
          vtree->titles[i] = g_strdup(titles[i]);

          ++i;
        }
    }

  if (GTK_WIDGET_VISIBLE(vtree))
    gtk_widget_queue_draw(GTK_WIDGET(vtree));
}

void           
gtk_vtree_structure_changed (GtkVTree* vtree)
{
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  if (vtree->structure_changed)
    {
      return;
    }
  else
    {
      vtree->structure_changed = TRUE;

      /* Clean up */
      if (vtree->onscreen_nodes != NULL)
        {
          guint i = 0;
          while (i < vtree->onscreen_nodes->len)
            {
              g_free(g_ptr_array_index(vtree->onscreen_nodes, i));
              
              ++i;
            }
      
          g_ptr_array_free(vtree->onscreen_nodes, TRUE);
          vtree->onscreen_nodes = NULL;
        }

      vtree->total_rows = 0;

      vtree->last_node = NULL;

      if (GTK_WIDGET_VISIBLE(vtree))
        gtk_widget_queue_draw(GTK_WIDGET(vtree));
    }
}

void 
gtk_vtree_data_changed (GtkVTree* vtree)
{
  if (GTK_WIDGET_VISIBLE(vtree))
    gtk_widget_queue_draw(GTK_WIDGET(vtree));
}

/* 
 * Widget methods begin here. 
 */

static void
gtk_vtree_destroy (GtkObject* object)
{
  GtkVTree* vtree;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_VTREE(object));

  vtree = GTK_VTREE(object);

  vtree->vtable = NULL;

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gtk_vtree_finalize (GtkObject* object)
{
  GtkVTree* vtree;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_VTREE(object));

  vtree = GTK_VTREE(object);

  gtk_vtree_set_scroll_adjustments(vtree, NULL, NULL);

  free_titles(vtree);

  if (vtree->onscreen_nodes)
    g_ptr_array_free(vtree->onscreen_nodes, TRUE);

  if (vtree->column_widths)
    g_free(vtree->column_widths);
  
  if (vtree->column_xpositions)
    g_free(vtree->column_xpositions);

  if (GTK_OBJECT_CLASS (parent_class)->finalize)
    (*GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
gtk_vtree_realize (GtkWidget* widget)
{
  GtkVTree* vtree;
  GdkWindowAttr attributes;
  GdkGCValues values;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));
  g_return_if_fail (!GTK_WIDGET_REALIZED(widget));

  vtree = GTK_VTREE(widget);

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget);
  attributes.event_mask |= (GDK_EXPOSURE_MASK |
			    GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK |
			    GDK_KEY_PRESS_MASK |
			    GDK_KEY_RELEASE_MASK);
  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  /* main window */
  widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
				   &attributes, attributes_mask);

  gdk_window_set_user_data (widget->window, vtree);

  widget->style = gtk_style_attach (widget->style, widget->window);

  gdk_window_set_background (widget->window,
			     &widget->style->base[GTK_STATE_NORMAL]);

  /* XOR GC */

  values.foreground = widget->style->white;
  values.function = GDK_XOR;
  values.subwindow_mode = GDK_INCLUDE_INFERIORS;
  vtree->xor_gc = gdk_gc_new_with_values (widget->window,
					  &values,
					  GDK_GC_FOREGROUND |
					  GDK_GC_FUNCTION |
					  GDK_GC_SUBWINDOW);

}

static void
gtk_vtree_unrealize (GtkWidget* widget)
{
  GtkVTree* vtree;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));

  vtree = GTK_VTREE(widget);

  if (vtree->xor_gc)
    {
      gdk_gc_destroy(vtree->xor_gc);
      vtree->xor_gc = NULL;
    }

  if (GTK_WIDGET_CLASS (parent_class)->unrealize)
    (* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static void
gtk_vtree_size_request (GtkWidget* widget,
                        GtkRequisition* requisition)
{
  GtkVTree* vtree;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));

  vtree = GTK_VTREE(widget);

  {
    gint width = vtree->column_spacing + vtree->frame_width; /* right-side spacing */

    guint i = 0;
    while (i < vtree->columns)
      {
        width += vtree->column_widths[i] + vtree->column_spacing;

        ++i;
      }

    /* Add right-side frame width */
    width += vtree->frame_width;

    requisition->width = width;
  }

  /* We have no real opinion on height. At least one row is nice, plus
     titles and space for the frame. */
  requisition->height = TITLE_HEIGHT(vtree) + vtree->frame_width +
    vtree->row_height + vtree->row_spacing + vtree->frame_width;
}

static void
gtk_vtree_size_allocate (GtkWidget* widget,
                         GtkAllocation* allocation)
{
  GtkVTree* vtree;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));

  vtree = GTK_VTREE(widget);

  widget->allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (widget->window,
			      allocation->x,
			      allocation->y,
			      allocation->width,
			      allocation->height);
    }

  {
    gint space_for_list = allocation->height - 
      (TITLE_HEIGHT(vtree) + 2*vtree->frame_width);

    vtree->list_height = MAX(space_for_list, 0);
  }

  update_list_width(vtree);

  /* Since the size has changed, we have to recompute our cached 
   * information about onscreen nodes.
   */
  gtk_vtree_structure_changed(vtree);
}

static gint
gtk_vtree_button_press (GtkWidget* widget,
                        GdkEventButton* event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  if (!GTK_WIDGET_HAS_FOCUS(widget))
    gtk_widget_grab_focus (widget);

  return FALSE;
}

static gint
gtk_vtree_button_release (GtkWidget* widget,
                          GdkEventButton* event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  return FALSE;
}

static gint
gtk_vtree_motion_notify (GtkWidget* widget,
                         GdkEventMotion* event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  return FALSE;
}

static gint
gtk_vtree_key_press (GtkWidget* widget,
                     GdkEventKey* event)
{
  GtkVTree* vtree;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  if (GTK_WIDGET_CLASS (parent_class)->key_press_event &&
      GTK_WIDGET_CLASS (parent_class)->key_press_event (widget, event))
    return TRUE;

  vtree = GTK_VTREE(widget);

  switch (event->keyval)
    {
    default:
      break;
    }

  return FALSE;
}

static void
gtk_vtree_style_set (GtkWidget* widget,
                     GtkStyle* previous_style)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));
  
}

static void
gtk_vtree_state_changed (GtkWidget* widget,
                         GtkStateType previous_state)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));

}

/*
 * Drawing Code
 */

static void 
retraverse_tree(GtkVTree* vtree)
{
  guint n_rows_onscreen;
  guint first_row_onscreen;

  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));
  g_return_if_fail(vtree->structure_changed);

  /* Don't do this next time we paint... */
  vtree->structure_changed = FALSE;

  g_assert(vtree->onscreen_nodes == NULL);

  vtree->onscreen_nodes = g_ptr_array_new();

  if (vtree->vtable->vtree_n_rows != NULL)
    {
      vtree->total_rows = N_ROWS(vtree);
      vtree->last_node = NTH_NODE(vtree, vtree->total_rows-1);
    }
  else 
    {
      /* FIXME, we will have to walk the whole tree. */
    }

  n_rows_onscreen = N_ROWS_ONSCREEN(vtree); /* How many we could fit */

  first_row_onscreen = ROW_AT_OFFSET(vtree, vtree->voffset);
    
  printf("We could fit %u nodes of %u\n", n_rows_onscreen, vtree->total_rows);

  if (vtree->vtable->vtree_nth_node != NULL)
    {
      guint i = first_row_onscreen;
      guint visible_rows_so_far = 0;

      while (i < vtree->total_rows && 
             visible_rows_so_far < n_rows_onscreen)
        {
          gpointer node;
          GtkVTreeNodeInfo public_info;
          NodeInfo* info;

          node = NTH_NODE(vtree, i);

          NODE_INFO(vtree, node, &public_info);

          info = g_new(NodeInfo, 1);
          
          info->node = node;
          info->depth = 0; /* Fixme */
          info->row = i;
          
          g_ptr_array_add(vtree->onscreen_nodes, info);
          
          ++visible_rows_so_far;
          
          ++i;
        }
  
      printf("%d nodes to draw\n", vtree->onscreen_nodes->len);
    }
  else 
    {
      /* FIXME */
    }

  setup_vadjustment(vtree);
}

static void 
draw_pixtext_cell(GtkVTree* vtree, NodeInfo* info, gint column, 
                  GdkRectangle* cellrect, GdkRectangle* clip)
{
  GdkPixmap* pixmap = NULL;
  GdkBitmap* bitmap = NULL;
  const gchar* text = NULL;

  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  NODE_GET_PIXTEXT(vtree, info->node, column, &pixmap, &bitmap, &text);

  {
    GdkGC* gc = GTK_WIDGET(vtree)->style->fg_gc[GTK_STATE_NORMAL];

    /* Place text 1 pixel above bottom of cell; of course it won't work 
     * if font is too big for the cell, in that case we just clip...
     */
    GdkFont* font = GTK_WIDGET(vtree)->style->font;
    gint font_y = cellrect->y + cellrect->height - font->descent - 1;
    gint font_x = cellrect->x;

    gdk_gc_set_clip_rectangle(gc, clip);

    if (pixmap != NULL)
      {
        gint w, h;
        gint vextra;

        gdk_window_get_size(pixmap, &w, &h);

        vextra = cellrect->height - h;

        if (vextra < 0)
          vextra = 0;

        gdk_draw_pixmap(GTK_WIDGET(vtree)->window,
                        gc,
                        pixmap,
                        0, 0,
                        cellrect->x,
                        cellrect->y + vextra/2,
                        w, h);

        font_x += w + 2; /* Leave two pixels before text */
      }

    if (text != NULL)
      {
#if 0
        printf("Drawing string %s with clip (%d,%d) %d x %d\n"
               "  cellrect: (%d,%d) %d x %d\n",
               text, clip->x, clip->y, clip->width, clip->height,
               cellrect->x, cellrect->y, cellrect->width, cellrect->height);
#endif

        gdk_draw_string(GTK_WIDGET(vtree)->window, 
                        GTK_WIDGET(vtree)->style->font,
                        gc,
                        font_x,
                        font_y,
                        text);
      }

    gdk_gc_set_clip_rectangle(gc, NULL);
  }
}

static void
xor_row_focus (GtkVTree* vtree, GdkRectangle* area, gint top_ypixel)
{
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  if (area)
    gdk_gc_set_clip_rectangle(vtree->xor_gc, area);

  gdk_draw_rectangle (GTK_WIDGET(vtree)->window, 
                      vtree->xor_gc, FALSE,
                      vtree->frame_width - vtree->hoffset, 
                      top_ypixel,
                      vtree->entire_list_width-1,
                      vtree->row_height-1);

  if (area)
    gdk_gc_set_clip_rectangle(vtree->xor_gc, NULL);
}

static void
draw_row(GtkVTree* vtree, GdkRectangle* area, 
         NodeInfo* info, GdkRectangle* rowrect,
         GtkVTreeCellType* celltypes)
{
  guint i;
  gint xpos;
  GdkRectangle cellrect;
  GdkRectangle intersection;

  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  /* draw the row background */

  if (gdk_rectangle_intersect (area, 
                               rowrect,
                               &intersection))
    {
      GdkGC* gc = GTK_WIDGET(vtree)->style->base_gc[GTK_STATE_ACTIVE];
      gdk_gc_set_clip_rectangle(gc, area);
      gdk_draw_rectangle (GTK_WIDGET(vtree)->window,
                          gc,
                          TRUE,
                          intersection.x,
                          intersection.y,
                          intersection.width,
                          intersection.height);
      gdk_gc_set_clip_rectangle(gc, NULL);
    }
  else 
    {
      /* Area does not overlap this row. */
      return;
    }

  /* Now draw each cell */

  cellrect.y = rowrect->y;
  cellrect.height = rowrect->height;

  i = 0;
  while (i < vtree->columns)
    {
      gint indent = ((vtree->tree_column == i) ? info->depth*vtree->level_indent : 0);

      cellrect.x = vtree->column_xpositions[i] - vtree->hoffset + indent;
      cellrect.width = vtree->column_widths[i];

      if (cellrect.width < indent)
        cellrect.width = 0;
      else 
        cellrect.width -= indent;

      if (gdk_rectangle_intersect (area, &cellrect,
				   &intersection))
        
        {
          switch (celltypes[i])
            {
            case GTK_VTREE_CELL_BLANK:
              break;
            case GTK_VTREE_CELL_PIXTEXT:
              draw_pixtext_cell(vtree, info, i, &cellrect, 
                                &intersection);
              break;
            case GTK_VTREE_CELL_TOGGLE:
              break;
            case GTK_VTREE_CELL_RADIO:
              break;
            case GTK_VTREE_CELL_USER:
              break;
            default:
              break;
            }
        }

      ++i;
    }

  /* Draw focus if needed */
  if (GTK_WIDGET_HAS_FOCUS(GTK_WIDGET(vtree)) &&
      info->node == GET_FOCUS_NODE(vtree))
    {
      xor_row_focus(vtree, area, rowrect->y);
    }
}

static void 
init_first_rowrect(GtkVTree* vtree, GdkRectangle* rowrect)
{
  gint tmp;

  NodeInfo* info = g_ptr_array_index(vtree->onscreen_nodes, 0);

  rowrect->y = ROW_TOPPIXEL(vtree, info->row);
  rowrect->height = vtree->row_height;
  rowrect->x = vtree->frame_width;
  rowrect->width = vtree->list_width;
}

/* NodeInfo* is the current row, not the one being stepped to.
 * May be used in the future, isn't now. 
 */
static void
step_rowrect(GtkVTree* vtree, NodeInfo* info, GdkRectangle* rowrect)
{
  rowrect->y += vtree->row_height + vtree->row_spacing;
}

static void
step_n_rowrects(GtkVTree* vtree, NodeInfo* info, guint n, GdkRectangle* rowrect)
{
  rowrect->y += (vtree->row_height + vtree->row_spacing) * n;
}

static void 
rowrect_for_node(GtkVTree* vtree, NodeInfo* info, GdkRectangle* rowrect)
{
  g_return_if_fail (vtree->onscreen_nodes != NULL);

  init_first_rowrect(vtree, rowrect);

  rowrect->y += (vtree->row_height + vtree->row_spacing) * (info->row - ((NodeInfo*)g_ptr_array_index(vtree->onscreen_nodes, 0))->row);
}

/* 
 * keep in sync with draw_single_node
 */
static void
draw_rows(GtkVTree* vtree, 
          GdkRectangle* area)
{
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  /* If the structure is "dirty" re-traverse it, then draw the rows 
   * in the area.
   */
  if (vtree->structure_changed)
    {
      retraverse_tree(vtree);
    }
  
  g_assert(!vtree->structure_changed);

  if (vtree->onscreen_nodes)
    {
      GdkRectangle subarea;
      GdkRectangle list_area;

      list_area.x = vtree->frame_width;
      list_area.y = TITLE_HEIGHT(vtree) + vtree->frame_width;
      list_area.width = vtree->list_width;
      list_area.height = vtree->list_height;

      if (gdk_rectangle_intersect(area, &list_area, 
                                  &subarea))
        {
          GdkRectangle rowrect;
          GtkVTreeCellType* celltypes;
          gpointer focus_node;
          guint i;

          celltypes = g_new(GtkVTreeCellType, vtree->columns);
    
          focus_node = GET_FOCUS_NODE(vtree);

          init_first_rowrect(vtree, &rowrect);

          i = 0;
          while (i < vtree->onscreen_nodes->len)
            {
              NodeInfo* info = g_ptr_array_index(vtree->onscreen_nodes, i);
          
              NODE_CELLTYPES(vtree, info->node, celltypes);

              /* Draw the row itself */

              draw_row(vtree, &subarea, info, &rowrect, celltypes);
          
              step_rowrect(vtree, info, &rowrect);

              ++i;
            }

          g_free(celltypes);
        }
    }            
}

/* 
 * keep in sync with draw_rows
 */
static void
draw_single_node(GtkVTree* vtree, 
                 gpointer node)
{
  g_return_if_fail(vtree != NULL);
  g_return_if_fail(GTK_IS_VTREE(vtree));

  printf("Drawing single node %p\n", node);

  /* If the structure is "dirty" re-traverse it 
   */
  if (vtree->structure_changed)
    {
      retraverse_tree(vtree);
    }
  
  g_assert(!vtree->structure_changed);

  if (vtree->onscreen_nodes)
    {
      GdkRectangle rowrect;
      GdkRectangle list_area;
      GdkRectangle subarea;

      NodeInfo* info = NULL;
      int i = 0;

      while (i < vtree->onscreen_nodes->len)
        {
          NodeInfo* maybe = g_ptr_array_index(vtree->onscreen_nodes, i);
          
          if (maybe->node == node)
            {
              info = maybe;
              break;
            }

          ++i;
        }

      if (info == NULL)
        return; // not onscreen

      rowrect_for_node(vtree, info, &rowrect);

      list_area.x = vtree->frame_width;
      list_area.y = TITLE_HEIGHT(vtree) + vtree->frame_width;
      list_area.width = vtree->list_width;
      list_area.height = vtree->list_height;

      if (gdk_rectangle_intersect(&rowrect, &list_area, 
                                  &subarea))
        {
          GtkVTreeCellType* celltypes;
          gpointer focus_node;

          celltypes = g_new(GtkVTreeCellType, vtree->columns);
    
          focus_node = GET_FOCUS_NODE(vtree);
               
          NODE_CELLTYPES(vtree, info->node, celltypes);
          
          /* Draw the row itself */
          
          draw_row(vtree, &subarea, info, &rowrect, celltypes);
          
          g_free(celltypes);
        }
    }
}

static void
gtk_vtree_paint (GtkWidget* widget,
                 GdkRectangle* area)
{
  GtkVTree* vtree;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_VTREE(widget));

  vtree = GTK_VTREE(widget);

  if (!GTK_WIDGET_DRAWABLE (widget))
    return;

  gdk_window_clear_area (widget->window,
                         area->x, 
                         area->y,
                         area->width, 
                         area->height);
  
  gtk_paint_shadow (widget->style, widget->window,
                    GTK_STATE_NORMAL, vtree->shadow_type,
                    area, widget, "vtree",
                    0, 0 + TITLE_HEIGHT(vtree), 
                    widget->allocation.width,
                    widget->allocation.height - TITLE_HEIGHT(vtree));
  
  draw_rows (vtree, area);
}

static void
gtk_vtree_draw (GtkWidget* widget,
                GdkRectangle* area)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_VTREE (widget));

  gtk_vtree_paint(widget, area);
}

static gint
gtk_vtree_expose (GtkWidget* widget,
                  GdkEventExpose* event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  gtk_vtree_paint(widget, &event->area);
  return FALSE;
}

/*
 * Moveto
 */

/* from CList: scroll the viewing area of the list to the given column
 * and row; row_align and col_align are between 0-1 representing the
 * location the row should appear on the screnn, 0.0 being top or
 * left, 1.0 being bottom or right; if row or column is -1 then then
 * there is no change 
 */

static void 
gtk_vtree_moveto (GtkVTree* vtree,
                  gint      row,
                  gint      column,
                  gfloat    row_align,
                  gfloat    col_align)
{
  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));

  printf("Moveto %d,%d (%g,%g)\n", row, column, row_align, col_align);

  if (column >= 0 && column < vtree->columns)
    {
      col_align = CLAMP (col_align, 0, 1);

      /* adjust horizontal scrollbar */
      if (vtree->hadjustment && column >= 0)
        {
          gint leftpos = vtree->column_xpositions[column];
          gint rightpos = vtree->column_xpositions[column] + vtree->list_width;
          
          gtk_adjustment_set_value(vtree->hadjustment, 
                                   leftpos + (rightpos - leftpos)*col_align);
        }
    }

  if (row >= 0 && row < vtree->total_rows)
    {
      row_align = CLAMP (row_align, 0, 1);
      
      /* adjust vertical scrollbar */
      if (vtree->vadjustment && row >= 0)
        {
          gint toppos = ROW_OFFSET(vtree, row);
          gint bottompos = toppos + vtree->list_height;
          gint maxpos = ENTIRE_LIST_HEIGHT(vtree);
          
          bottompos = MIN(bottompos, maxpos);

          gtk_adjustment_set_value(vtree->vadjustment,
                                   toppos + (toppos - bottompos)*row_align);
        }
    }
}

static void 
gtk_vtree_moveto_percent(GtkVTree* vtree, 
                         gfloat xpercent,
                         gfloat ypercent)
{
  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));

  if (xpercent >= 0 && xpercent <= 1)
    {
      /* adjust horizontal scrollbar */
      if (vtree->hadjustment != NULL)
        {
          gint maxpos = vtree->entire_list_width;
          
          gtk_adjustment_set_value(vtree->hadjustment,
                                   maxpos*xpercent);
        }
    }

  if (ypercent >= 0 && ypercent <= 1)
    {
      /* adjust vertical scrollbar */
      if (vtree->vadjustment != NULL)
        {
          gint maxpos = ENTIRE_LIST_HEIGHT(vtree);
          
          gtk_adjustment_set_value(vtree->vadjustment,
                                   maxpos*ypercent);
        }
    }
}

/*
 * Focus stuff
 */

/**************
 * See CList for this focus stuff; need to use a key-signal binding, 
 * since the arrow keys won't make it to the key event handler. 
 */

static void
gtk_vtree_xor_focus (GtkVTree* vtree)
{
  GtkWidget* widget;
  gpointer node;

  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));

  widget = GTK_WIDGET(vtree);

  if (!GTK_WIDGET_DRAWABLE(widget))
    return;

  g_return_if_fail(!vtree->structure_changed); /* not sure this should happen... */
  if (vtree->structure_changed)
    return; /* don't bother */

  node = GET_FOCUS_NODE(vtree);

  if (node != NULL)
    {
      guint i;
      GdkRectangle rowrect;

      /* Linear search onscreen nodes for the focused node; 
       * should be fast enough. no more than 40 or 50 nodes, max.
       */

      init_first_rowrect(vtree, &rowrect);

      i = 0;
      while (i < vtree->onscreen_nodes->len)
        {
          NodeInfo* info;

          info = g_ptr_array_index(vtree->onscreen_nodes, i);

          if (info->node == node)
            break;
          
          step_rowrect(vtree, info, &rowrect);

          ++i;
        }

      if (i < vtree->onscreen_nodes->len)
        {
          printf("XOR node %p\n", node);
          xor_row_focus(vtree, NULL, rowrect.y);
        }
    }
}

static gint
gtk_vtree_focus_in (GtkWidget* widget,
                    GdkEventFocus* event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  GTK_WIDGET_SET_FLAGS (widget, GTK_HAS_FOCUS);

  /* Draws the focus */
  gtk_widget_queue_draw(widget);

  return FALSE;
}

static gint
gtk_vtree_focus_out (GtkWidget* widget,
                     GdkEventFocus* event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_VTREE (widget), FALSE);

  GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS);

  /* Erases the focus */
  gtk_widget_queue_draw(widget);

  return FALSE;
}

/*
 * Adjustments and scrolling
 */

/* Update adjustments to reflect our current size */
static void
setup_vadjustment (GtkVTree *vtree)
{
  if (vtree->structure_changed) /* May as well wait until we do the update */
    return;

  if (vtree->vadjustment)
    {
      vtree->vadjustment->page_size = vtree->list_height;
      vtree->vadjustment->page_increment = vtree->list_height/2;
      vtree->vadjustment->step_increment = vtree->row_height;
      vtree->vadjustment->lower = 0;
      vtree->vadjustment->upper = ENTIRE_LIST_HEIGHT (vtree);

      {
        float oldvalue = vtree->vadjustment->value;

        vtree->vadjustment->value = CLAMP(vtree->vadjustment->value,
                                          vtree->vadjustment->lower,
                                          vtree->vadjustment->upper);

        if (vtree->vadjustment->value != oldvalue)
          {
            gtk_signal_emit_by_name (GTK_OBJECT (vtree->vadjustment),
                                     "value_changed");
          }
      }

      gtk_signal_emit_by_name (GTK_OBJECT (vtree->vadjustment), "changed");
    }
}

static void
setup_hadjustment (GtkVTree *vtree)
{
  if (vtree->hadjustment)
    {
      vtree->hadjustment->page_size = vtree->list_width;
      vtree->hadjustment->page_increment = vtree->list_width / 2;
      vtree->hadjustment->step_increment = vtree->list_width / 10;
      vtree->hadjustment->lower = 0;
      vtree->hadjustment->upper = vtree->entire_list_width;

      printf("%d vis. width, %d entire\n", 
             vtree->list_width, 
             vtree->entire_list_width);

      {
        float oldvalue = vtree->hadjustment->value;

        vtree->hadjustment->value = CLAMP(vtree->hadjustment->value,
                                          vtree->hadjustment->lower,
                                          vtree->hadjustment->upper);

        if (vtree->hadjustment->value != oldvalue)
          {
            gtk_signal_emit_by_name (GTK_OBJECT (vtree->hadjustment),
                                     "value_changed");
          }
      }

      gtk_signal_emit_by_name (GTK_OBJECT (vtree->hadjustment), "changed");
    }
}

static void
vadjustment_changed (GtkAdjustment *adjustment,
		     gpointer       data)
{
  GtkVTree* vtree;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);

  vtree = GTK_VTREE (data);
}

static void
hadjustment_changed (GtkAdjustment *adjustment,
		     gpointer       data)
{
  GtkVTree *vtree;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);

  vtree = GTK_VTREE (data);
}

static void
vadjustment_value_changed (GtkAdjustment *adjustment,
			   gpointer       data)
{
  GtkVTree* vtree;
  GdkRectangle area;
  gint diff, value;
  gint oldoffset;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);
  g_return_if_fail (GTK_IS_VTREE (data));

  vtree = GTK_VTREE (data);

  if (!GTK_WIDGET_DRAWABLE (vtree) || adjustment != vtree->vadjustment)
    return;

  value = adjustment->value;

  g_return_if_fail(value >= 0.0);
  g_return_if_fail(value <= ENTIRE_LIST_HEIGHT(vtree));
  
  oldoffset = vtree->voffset;

  vtree->voffset = (gint) value;

  /* Update cached set of rows */
  if (vtree->voffset != oldoffset)
    gtk_vtree_structure_changed(vtree);
}

static void
hadjustment_value_changed (GtkAdjustment *adjustment,
			   gpointer       data)
{
  GtkVTree* vtree;
  GdkRectangle area;
  gint diff, value;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);
  g_return_if_fail (GTK_IS_VTREE (data));

  vtree = GTK_VTREE (data);

  if (!GTK_WIDGET_DRAWABLE (vtree) || adjustment != vtree->hadjustment)
    return;

  value = adjustment->value;

  g_return_if_fail(value >= 0.0);
  g_return_if_fail(value <= vtree->entire_list_width);
  
  vtree->hoffset = (gint) value;

  if (GTK_WIDGET_VISIBLE(GTK_WIDGET(vtree)))
    gtk_widget_queue_draw(GTK_WIDGET(vtree));
}

/*******/

void
gtk_vtree_set_hadjustment (GtkVTree      *vtree,
			   GtkAdjustment *adjustment)
{
  GtkAdjustment *old_adjustment;

  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));
  if (adjustment)
    g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
  
  if (vtree->hadjustment == adjustment)
    return;
  
  old_adjustment = vtree->hadjustment;

  if (vtree->hadjustment)
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (vtree->hadjustment), vtree);
      gtk_object_unref (GTK_OBJECT (vtree->hadjustment));
    }

  vtree->hadjustment = adjustment;

  if (vtree->hadjustment)
    {
      gtk_object_ref (GTK_OBJECT (vtree->hadjustment));
      gtk_object_sink (GTK_OBJECT (vtree->hadjustment));

      gtk_signal_connect (GTK_OBJECT (vtree->hadjustment), "changed",
			  (GtkSignalFunc) hadjustment_changed,
			  (gpointer) vtree);
      gtk_signal_connect (GTK_OBJECT (vtree->hadjustment), "value_changed",
			  (GtkSignalFunc) hadjustment_value_changed,
			  (gpointer) vtree);
    }

  setup_hadjustment(vtree);
}

GtkAdjustment *
gtk_vtree_get_hadjustment (GtkVTree *vtree)
{
  g_return_val_if_fail (vtree != NULL, NULL);
  g_return_val_if_fail (GTK_IS_VTREE (vtree), NULL);

  return vtree->hadjustment;
}

void
gtk_vtree_set_vadjustment (GtkVTree      *vtree,
			   GtkAdjustment *adjustment)
{
  GtkAdjustment *old_adjustment;

  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));
  if (adjustment)
    g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));

  if (vtree->vadjustment == adjustment)
    return;
  
  old_adjustment = vtree->vadjustment;

  if (vtree->vadjustment)
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (vtree->vadjustment), vtree);
      gtk_object_unref (GTK_OBJECT (vtree->vadjustment));
    }

  vtree->vadjustment = adjustment;

  if (vtree->vadjustment)
    {
      gtk_object_ref (GTK_OBJECT (vtree->vadjustment));
      gtk_object_sink (GTK_OBJECT (vtree->vadjustment));

      gtk_signal_connect (GTK_OBJECT (vtree->vadjustment), "changed",
			  (GtkSignalFunc) vadjustment_changed,
			  (gpointer) vtree);
      gtk_signal_connect (GTK_OBJECT (vtree->vadjustment), "value_changed",
			  (GtkSignalFunc) vadjustment_value_changed,
			  (gpointer) vtree);
    }

  setup_vadjustment(vtree);
}

GtkAdjustment *
gtk_vtree_get_vadjustment (GtkVTree *vtree)
{
  g_return_val_if_fail (vtree != NULL, NULL);
  g_return_val_if_fail (GTK_IS_VTREE (vtree), NULL);

  return vtree->vadjustment;
}

static void
gtk_vtree_set_scroll_adjustments (GtkVTree      *vtree,
				  GtkAdjustment *hadjustment,
				  GtkAdjustment *vadjustment)
{
  if (vtree->hadjustment != hadjustment)
    gtk_vtree_set_hadjustment (vtree, hadjustment);
  if (vtree->vadjustment != vadjustment)
    gtk_vtree_set_vadjustment (vtree, vadjustment);
}

/* 
 * Bindings 
 */

static void 
change_focus(GtkVTree* vtree, gpointer old, gpointer new)
{
  g_return_if_fail(vtree != 0);
  g_return_if_fail(GTK_IS_VTREE (vtree));
  g_return_if_fail(old != NULL || new != NULL);

  printf("Changing focus from %p to %p\n", old, new);

  if (old == new)
    return;

  if (old != NULL)
    {
      g_return_if_fail(GET_FOCUS_NODE(vtree) != NULL);
      NODE_FOCUS_OUT(vtree, old);
      draw_single_node(vtree, old);
    }

  if (new != NULL)
    {
      NODE_FOCUS_IN(vtree, new);
      draw_single_node(vtree, new);
      g_return_if_fail(GET_FOCUS_NODE(vtree) != NULL);
    }
}

static void
move_focus_row (GtkVTree      *vtree,
		GtkScrollType  scroll_type,
		gfloat         position)
{
  GtkWidget *widget;
  gpointer focus_node;

  g_return_if_fail (vtree != 0);
  g_return_if_fail (GTK_IS_VTREE (vtree));
  g_return_if_fail (!vtree->structure_changed);

  widget = GTK_WIDGET (vtree);
  focus_node = GET_FOCUS_NODE(vtree);

  switch (scroll_type)
    {
    case GTK_SCROLL_STEP_BACKWARD:
      {
        gpointer prev = prev_row_node(vtree, focus_node); 
        change_focus(vtree, focus_node, prev);
      }
      break;

    case GTK_SCROLL_STEP_FORWARD:
      {
        gpointer next = next_row_node(vtree, focus_node);
        change_focus(vtree, focus_node, next);
      }
      break;

    case GTK_SCROLL_PAGE_BACKWARD:
      {
        /* This is hugely inefficient due to the prev_row_node
         * implementation FIXME
         */
        gint i = vtree->onscreen_nodes->len/2;
        gpointer prev = prev_row_node(vtree, focus_node);
        while (i != 0 && prev != NULL)
          {
            prev = prev_row_node(vtree, prev);
            
            --i;
          }

        change_focus(vtree, focus_node, prev);
      }
      break;

    case GTK_SCROLL_PAGE_FORWARD:
      {
        gint i = 0;
        gint toscroll = vtree->onscreen_nodes->len/2;
        gpointer next = next_row_node(vtree, focus_node);
        while (i < toscroll && next != NULL)
          {
            next = next_row_node(vtree, focus_node);
            ++i;
          }
        change_focus(vtree, focus_node, next);
      }
      break;

    case GTK_SCROLL_JUMP:
      if (position >= 0 && position <= 1)
	{
          gint new_row = vtree->total_rows*position;
          gpointer new_node = scan_to_row(vtree, new_row);
          change_focus(vtree, focus_node, new_node);
	}
      break;
    default:
      break;
    }
}

static void
gtk_vtree_select_node          (GtkVTree       *vtree,
                                gpointer        node,
                                GdkEvent       *event)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_unselect_node        (GtkVTree       *vtree,
                                gpointer        node,
                                GdkEvent       *event)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_row_move            (GtkVTree       *vtree,
                               gint            source_row,
                               gint            dest_row)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_click_column        (GtkVTree       *vtree,
                               gint            column)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_resize_column       (GtkVTree       *vtree,
                               gint            column,
                               gint            width)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_toggle_focus_row    (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_select_all          (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_unselect_all        (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_undo_selection      (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_start_selection     (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_end_selection       (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_extend_selection    (GtkVTree       *vtree,
                               GtkScrollType   scroll_type,
                               gfloat          position,
                               gboolean        auto_start_selection)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_scroll_horizontal   (GtkVTree       *vtree,
                               GtkScrollType   scroll_type,
                               gfloat          position)
{
  gint column = -1;

  g_warning("%s", __FUNCTION__);

  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));

  /* What is this for? -hp */
  if (gdk_pointer_is_grabbed () && 
      GTK_WIDGET_HAS_GRAB (vtree))
    return;

  switch (scroll_type)
    {
    case GTK_SCROLL_STEP_BACKWARD:
      column = closest_column_at_xpixel(vtree, vtree->frame_width);
      if (column > 0)
        {
          --column;
          gtk_vtree_moveto(vtree, -1, column, 0.0, 0.0);
        }
      else
        gtk_vtree_moveto_percent(vtree, 0.0, -1); /* go to start */
      break;

    case GTK_SCROLL_STEP_FORWARD:
      column = closest_column_at_xpixel(vtree, vtree->frame_width);
      if (column < (vtree->columns-1))
        {
          ++column;
          gtk_vtree_moveto(vtree, -1, column, 0.0, 0.0);
        }
      else
        gtk_vtree_moveto_percent(vtree, 1.0, -1); /* go to end */
      break;

    case GTK_SCROLL_PAGE_BACKWARD:
    case GTK_SCROLL_PAGE_FORWARD:
      return;

    case GTK_SCROLL_JUMP:
      if (position >= 0 && position <= 1)
	{
          
	}
      else
	return;
      break;
    default:
      break;
    }
}

static void
gtk_vtree_scroll_vertical     (GtkVTree       *vtree,
                               GtkScrollType   scroll_type,
                               gfloat          position)
{
  gpointer old_focus_node;
  gpointer new_focus_node;

  g_warning("%s", __FUNCTION__);

  g_return_if_fail (vtree != NULL);
  g_return_if_fail (GTK_IS_VTREE (vtree));

  /* What is this for? -hp */
  if (gdk_pointer_is_grabbed () && 
      GTK_WIDGET_HAS_GRAB (vtree))
    return;

  switch (vtree->selection_mode)
    {
    case GTK_SELECTION_EXTENDED:
      g_warning("extended selection not implemented");
      break;

    case GTK_SELECTION_BROWSE:
      g_warning("browse selection not implemented");
      break;

    default:
      move_focus_row (vtree, scroll_type, position);

      g_warning("FIXME: move focus row onscreen if it's off the top or bottom");

      break;
    }
}

static void
gtk_vtree_toggle_add_mode     (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

static void
gtk_vtree_abort_column_resize (GtkVTree       *vtree)
{
  g_warning("%s", __FUNCTION__);
}

/*
 * Misc. stuff, including tree traversal
 */

static gpointer 
next_row_node (GtkVTree* vtree, gpointer cur)
{
  GtkVTreeNodeInfo info;
  gpointer next = NULL;

  if (cur == NULL)
    return NODE_FIRST(vtree);

  NODE_INFO(vtree, cur, &info);
  
  if (info.state == GTK_VTREE_EXPANDED)
    next = NODE_FIRST_CHILD(vtree, cur);
  else
    next = NODE_NEXT(vtree, cur);

  return next;
}

static gpointer 
prev_row_node (GtkVTree* vtree, gpointer cur)
{
  GtkVTreeNodeInfo info;
  gpointer prev = NULL;

  if (cur == NULL)
    return vtree->last_node;

  prev = NODE_PREV(vtree, cur);

  if (prev == NULL)
    {
      return NULL;
    }
  else 
    {
      /* For each node, scan to last child, then start
       * scanning last child's children
       */
      gpointer last_seen = prev;
      while (TRUE)
        {
          gpointer possible;
          
          NODE_INFO(vtree, last_seen, &info);
          
          if (info.state == GTK_VTREE_EXPANDED)
            {
              possible = NODE_FIRST_CHILD(vtree, last_seen);
              
              while (possible != NULL)
                {
                  last_seen = possible;
                  possible = NODE_NEXT(vtree, last_seen);
                }
            }
          else
            break;
        }
           
      g_assert(last_seen != NULL); 

      return last_seen;
    } 
}


static gpointer 
scan_to_row (GtkVTree* vtree, guint row)
{
  if (vtree->vtable->vtree_nth_node)
    {
      return NTH_NODE(vtree, row);
    }
  else
    {
      g_warning("%s not implemented", __FUNCTION__);
      return NULL;
    }
}

static gint 
column_at_xpixel(GtkVTree* vtree, gint xpixel)
{
  int i = 0;
  while (i < vtree->columns)
    {
      gint real_pos = vtree->column_xpositions[i] - vtree->hoffset;
      gint end_pos = real_pos + vtree->column_widths[i];

      if (xpixel >= real_pos && xpixel <= end_pos)
        return i;

      ++i;
    }

  return -1;
}

static gint
closest_column_at_xpixel(GtkVTree* vtree, gint xpixel)
{
  int i = 0;
  while (i < vtree->columns)
    {
      gint real_pos = vtree->column_xpositions[i] - vtree->hoffset;
      gint end_pos = real_pos + vtree->column_widths[i] + vtree->column_spacing/2;

      if (xpixel <= end_pos) /* may be off the left */
        return i;

      ++i;
    }

  return vtree->columns - 1; /* off the right - choose last column */
}

/* Not exactly efficient */
static gboolean 
node_is_onscreen(GtkVTree* vtree, gpointer node)
{
  if (vtree->onscreen_nodes)
    {
      NodeInfo* info = NULL;
      int i = 0;

      while (i < vtree->onscreen_nodes->len)
        {
          NodeInfo* maybe = g_ptr_array_index(vtree->onscreen_nodes, i);
          
          if (maybe->node == node)
            {
              info = maybe;
              break;
            }

          ++i;
        }

      if (info == NULL)
        return FALSE;
      else
        return TRUE;
    }
  else
    return FALSE;
}


