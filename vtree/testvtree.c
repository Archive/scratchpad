
#include "gtkvtree.h"
#include <gtk/gtk.h>

/* XPM */
static char * package_xpm[] = {
"16 12 35 1",
" 	c None",
".	c #000000",
"+	c #D8CCA8",
"@	c #696351",
"#	c #3E3229",
"$	c #261F19",
"%	c #B3A98B",
"&	c #555042",
"*	c #474337",
"=	c #716B58",
"-	c #5D5243",
";	c #9C9479",
">	c #958D74",
",	c #F1ECE0",
"'	c #E3DBC2",
")	c #6D6452",
"!	c #443F33",
"~	c #ABA185",
"{	c #7A6B58",
"]	c #CCA070",
"^	c #F5EEE6",
"/	c #54493E",
"(	c #7D7661",
"_	c #5E5949",
":	c #382D25",
"<	c #685444",
"[	c #504031",
"}	c #888077",
"|	c #F5F1E9",
"1	c #5E4C3D",
"2	c #281F19",
"3	c #E5D0B9",
"4	c #4E3F33",
"5	c #261E18",
"6	c #5A493B",
"       ...      ",
"     ..++@#$    ",
"   ..%&*=-;>..  ",
"  .,'+%)!*~%{.  ",
"  .]^,/(+(_:<.  ",
"  .]][}|'+12<.  ",
"  .]][]]31<2<.  ",
"  .]][]]]1<24.  ",
"  .]][]]]1<5.   ",
"   ..[]]]1<.    ",
"     ..]]6.     ",
"       ...      "};


/* To start with, we'll test the VTree in list mode */

#define N_LISTNODES 10000

typedef struct _ListNode ListNode;

struct _ListNode {
  gchar* name;
  gchar* number;
};
  
static ListNode* listnodes = NULL;

static void 
list_vtree_node_info (GtkVTree* vtree,
                      gpointer node, 
                      GtkVTreeNodeInfo* info)
{
  info->state = GTK_VTREE_NOCHILDREN;
}

static gpointer 
list_vtree_node_first (GtkVTree* vtree)
{
  /* We use array index + 1 to avoid returning NULL, which would mean 
   * the tree has no nodes. This is not strictly portable since NULL
   * does not have to be 0. To really be portable I guess you'd 
   * check the value of NULL and avoid that index. But this is a 
   * stupid test program.
   */
  return GUINT_TO_POINTER(1);
}
  
static gpointer 
list_vtree_node_next (GtkVTree* vtree, gpointer node)
{
  guint next = GPOINTER_TO_UINT(node) + 1;

  if (next > N_LISTNODES) /* Remember that we added one */
    return NULL;
  else 
    return GUINT_TO_POINTER(next);
}

static gpointer 
list_vtree_node_prev (GtkVTree* vtree, gpointer node)
{
  gint prev = (gint) GPOINTER_TO_UINT(node) - 1;

  if (prev < 0)
    return NULL;
  else 
    return GUINT_TO_POINTER((guint)prev);
}

static guint 
list_vtree_n_rows (GtkVTree* vtree)
{
  return N_LISTNODES;
}

  /* Return visible node number n, or NULL if none */
static gpointer 
list_vtree_nth_node (GtkVTree* vtree, guint n)
{
  return GUINT_TO_POINTER(n + 1);
}

static void 
list_vtree_node_celltypes (GtkVTree* vtree, 
                           gpointer node, 
                           GtkVTreeCellType* fillme)
{
  fillme[0] = GTK_VTREE_CELL_PIXTEXT;
  fillme[1] = GTK_VTREE_CELL_PIXTEXT;
}

static GdkPixmap* global_pixmap = NULL;
static GdkPixmap* global_mask = NULL;

static void
list_vtree_node_get_pixtext (GtkVTree* vtree,
                             gpointer node, guint column,
                             GdkPixmap** pixmap, 
                             GdkBitmap** mask,
                             const gchar** text)
{
  guint n = GPOINTER_TO_UINT(node) - 1;
  
  switch (column) {
  case 0:
    *text = listnodes[n].name;
    break;
  case 1:
    *text = listnodes[n].number;
    break;
  default:
    g_warning("oops, got column %u", column);
    break;
  }
  
  *pixmap = global_pixmap;
  *mask = global_mask;

  return;
}

/* -1 for none */
gint focused_row = 0;

static gpointer
list_vtree_get_focus_node (GtkVTree* vtree)
{
  /* notice that since -1 means no row, we properly return NULL in
     that case. */
  return GUINT_TO_POINTER((guint)focused_row) + 1;
}                           

static void 
list_vtree_node_focus_in (GtkVTree* vtree, gpointer node)
{
  g_return_if_fail(node != NULL);

  focused_row = (gint) GPOINTER_TO_UINT(node) - 1;
}

static void
list_vtree_node_focus_out (GtkVTree* vtree, gpointer node)
{
  g_return_if_fail(node != NULL);

  focused_row = -1;
}

GtkVTreeVTable list_vtable = {
  list_vtree_node_info,
  list_vtree_node_first,
  NULL,
  NULL,
  list_vtree_node_next,
  list_vtree_node_prev,
  NULL,
  list_vtree_n_rows,
  list_vtree_nth_node,
  list_vtree_node_celltypes,
  NULL,
  NULL,
  list_vtree_node_get_pixtext,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  list_vtree_get_focus_node,
  list_vtree_node_focus_in,
  list_vtree_node_focus_out
};

static gint delete_event_cb(GtkWidget* w, GdkEventAny* e, gpointer data);

int 
main(int argc, char* argv[])
{
  GtkWidget* window;
  GtkWidget* vtree;
  GtkWidget* sw;

  gtk_init(&argc, &argv);  

  listnodes = g_new0(ListNode, N_LISTNODES);

  printf("Generating %d nodes... ", N_LISTNODES);
  {
    guint i = 0;
    while (i < N_LISTNODES)
      {
        gchar buf[128];
        
        listnodes[i].name = "Node";
        
        g_snprintf(buf, 128, "%u", i);
        
        listnodes[i].number = g_strdup(buf);
        
        ++i;
      }
  }
  printf("done.\n");

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  sw = gtk_scrolled_window_new(NULL,NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  gtk_window_set_default_size(GTK_WINDOW(window), 300, 500);

  vtree = gtk_vtree_new(&list_vtable, 2);

  gtk_container_add(GTK_CONTAINER(window), sw);
  gtk_container_add(GTK_CONTAINER(sw), vtree);

  gtk_window_set_title(GTK_WINDOW(window), "VTree Test - List Mode");

  gtk_signal_connect(GTK_OBJECT(window),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  global_pixmap = gdk_pixmap_colormap_create_from_xpm_d(NULL,
                                                        gtk_widget_get_colormap(vtree),
                                                        &global_mask,
                                                        NULL,
                                                        package_xpm);

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}

static gint 
delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data)
{
  gtk_main_quit();
  return FALSE;
}









