
#include <gnome.h>
#include "gnome-canvas-arc.h"

static gint delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data);

int 
main(int argc, char** argv)
{
  GtkWidget* window;
  GtkWidget* canvas;

  gtk_init(&argc, &argv);  
  gdk_rgb_init();

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_widget_push_visual(gdk_rgb_get_visual());
  gtk_widget_push_colormap(gdk_rgb_get_cmap());
  canvas = gnome_canvas_new_aa(); /* _aa() */
  gtk_widget_pop_visual();
  gtk_widget_pop_colormap();

  gtk_container_add(GTK_CONTAINER(window), canvas);

  gtk_window_set_title(GTK_WINDOW(window), "Canvas Arc Test");
  gtk_window_set_default_size(GTK_WINDOW(window), 400, 600);
  gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, TRUE);
  gtk_container_set_border_width(GTK_CONTAINER(canvas), 10);

  gtk_signal_connect(GTK_OBJECT(window),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), 
                                 -200.0, -200.0, 200.0, 200.0);

  {
    GnomeCanvasItem* arc;

#if 0
    arc = gnome_canvas_item_new(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas)->root),
                                gnome_canvas_arc_get_type(),
                                "x1", -190.0, "y1", -190.0,
                                "x2", 0.0, "y2", 0.0,
                                "fill_color", "blue",
                                "start", 180.0, "distance", 95.0,
                                NULL);
#endif

#if 1
    arc = gnome_canvas_item_new(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas)->root),
                                gnome_canvas_arc_get_type(),
                                "x1", -190.0, "y1", -190.0,
                                "x2", 0.0, "y2", 0.0,
                                "fill_color", "purple",
                                "start", 275.0, "distance", 265.0,
                                /*                                "outline_color", "black", */
                                NULL);
#endif

#if 1
    arc = gnome_canvas_item_new(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas)->root),
                                gnome_canvas_arc_get_type(),
                                "x1", 20.0, "y1", 20.0,
                                "x2", 170.0, "y2", 170.0,
                                "fill_color", "red",
                                "start", 10.0, "distance", 280.0,
                                NULL);
#endif

#if 0
    arc = gnome_canvas_item_new(GNOME_CANVAS_GROUP(GNOME_CANVAS(canvas)->root),
                                gnome_canvas_arc_get_type(),
                                "x1", 20.0, "y1", 20.0,
                                "x2", 170.0, "y2", 170.0,
                                "fill_color", "green",
                                /*                                "outline_color", "black", */
                                "start", 290.0, "distance", 90.0,
                                NULL);
#endif    

  }

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}



static gint 
delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data)
{
  gtk_main_quit();
  return FALSE;
}


