#include <gdk/gdkprivate.h>
#include <gdk/gdkx.h>
#include <gdk/gdk.h>
#include <X11/Xlib.h>

#include "smallicon.h"

void
set_small_icon(GdkWindow *window,
	       GdkPixmap *pixmap,
	       GdkBitmap *mask)
{
  GdkAtom icon_atom;
  long data[2];
 
  g_return_if_fail (window != NULL);
  g_return_if_fail (pixmap != NULL);

  data[0] = ((GdkPixmapPrivate *)pixmap)->xwindow;
  if (mask)
	  data[1] = ((GdkPixmapPrivate *)mask)->xwindow;
  else
	  data[1] = 0;

  icon_atom = gdk_atom_intern ("KWM_WIN_ICON", FALSE);
  gdk_property_change (window, icon_atom, icon_atom, 
		       32,GDK_PROP_MODE_REPLACE,
		       (guchar *)data, 2);
  
}
