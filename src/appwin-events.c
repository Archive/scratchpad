#include "testbed.h"

/**** appwin_delete_event
 */
gboolean
appwin_delete_event(GtkWidget *widget,
		    GdkEventAny *event,
		    TestbedApp *appinfo)
{
  return appwin_menu_file_quit(widget, appinfo); /* go ahead and die, for now */
}

/****
     Outputs: TRUE if app can quit, FALSE otherwise
 */
gboolean
appwin_menu_file_quit(GtkWidget *widget,
		      TestbedApp *appinfo)
{
  gtk_main_quit();
  return TRUE;
}

void
appwin_menu_file_new(GtkWidget *widget,
		     TestbedApp *appinfo)
{
  GnomeCanvasItem *newpage;

  appinfo->page =
  newpage = gnome_canvas_item_new(gnome_canvas_root(GNOME_CANVAS(appinfo->canvas)),
				  page_item_get_type(),
				  "PageItem::x", 1.0, "PageItem::y", 1.0,
				  "PageItem::width", 8.5,
				  "PageItem::height", 11.0,
				  "PageItem::horizontal_margins", 1.0,
				  "PageItem::vertical_margins", 1.0,
				  "PageItem::display_guides", TRUE,
				  "PageItem::grid_start_y", 1.0,
				  "PageItem::gridline_spacing_y", 0.5,
				  "PageItem::grid_start_x", 0.0,
				  "PageItem::gridline_spacing_x", 0.5,
				  NULL);
}

void appwin_toggle_grids(GtkWidget *widget, TestbedApp *appinfo)
{
  if(!appinfo->page)
    return;

  gnome_canvas_item_set(appinfo->page,
			"PageItem::display_grid_horizontal",
			GTK_TOGGLE_BUTTON(widget)->active,
			"PageItem::display_grid_vertical",
			GTK_TOGGLE_BUTTON(widget)->active,
			NULL);
}

void appwin_toggle_guides(GtkWidget *widget, TestbedApp *appinfo)
{
  if(!appinfo->page)
    return;

  gnome_canvas_item_set(appinfo->page, "PageItem::display_guides",
			GTK_TOGGLE_BUTTON(widget)->active, NULL);
}
