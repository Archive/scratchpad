#ifndef PAGEITEM_H
#define PAGEITEM_H 1

#include <gnome.h>

BEGIN_GNOME_DECLS

#define PAGE_ITEM_TYPE (page_item_get_type())
#define PAGE_ITEM(obj)          (GTK_CHECK_CAST ((obj), PAGE_ITEM_TYPE, PageItem))
#define PAGE_ITEM_CLASS(klass)  (GTK_CHECK_CLASS_CAST ((klass), PAGE_ITEM_TYPE, PageItemClass))
#define IS_PAGE_ITEM(obj)         (GTK_CHECK_TYPE ((obj), PAGE_ITEM_TYPE))
#define IS_PAGE_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), PAGE_ITEM_TYPE))

typedef struct _PageItem      PageItem;
typedef struct _PageItemClass PageItemClass;

struct _PageItem {
  GnomeCanvasItem parent;

  gdouble x, y, width, height, hmargin, vmargin;
  gdouble grid_startx, grid_starty, grid_intervalx, grid_intervaly;

  GSList *guides;

  gulong fill_pixel, outline_pixel, guide_pixel;
  GdkGC *fill_gc, *outline_gc, *guide_gc;
  gint snap_threshhold;

  /*gboolean*/
  guchar snapping_set,
    snap_to_grid_set,
    display_guides,
    display_grid_h,
    display_grid_v;
};

struct _PageItemClass {
  GnomeCanvasItemClass _parent_class;

  GnomeCanvasItemClass *parent_class;
};

GtkType page_item_get_type(void);

GtkWidget *page_item_new(void);

void page_item_add_guide(PageItem *page_item,
			 GtkOrientation orientation,
			 gdouble x, gdouble y,
			 gdouble length);
void page_item_snap_item(PageItem *page_item,
			 gulong x1, gulong y1, gulong x2, gulong y2,
			 glong *dx, glong *dy);

END_GNOME_DECLS

#endif
