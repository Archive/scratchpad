#include "testbed.h"

GnomeUIInfo file_menu[] = {
  GNOMEUIINFO_ITEM_STOCK("_New", "Add a new page", appwin_menu_file_new,
			 GNOME_STOCK_MENU_NEW),
  GNOMEUIINFO_ITEM_STOCK("E_xit", "Exit the program", appwin_menu_file_quit,
			 GNOME_STOCK_MENU_QUIT),
  GNOMEUIINFO_END
};
GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_SUBTREE("_File", file_menu),
  GNOMEUIINFO_END
};

int main(int argc, char *argv[])
{
  GtkWidget *vbox, *table, *w;
  TestbedApp myapp;

  gnome_init("testbed", VERSION, argc, argv);

  /**
   * create main app window
   **/
  memset(&myapp, 0, sizeof(myapp));
  myapp.app = gnome_app_new("testbed", "Testbed");
  gtk_window_set_wmclass(GTK_WINDOW(myapp.app), "main_window","testbed");

  gtk_signal_connect(GTK_OBJECT(myapp.app), "delete_event",
		     GTK_SIGNAL_FUNC(appwin_delete_event), &myapp);

  gnome_app_create_menus_with_data(GNOME_APP(myapp.app),
				   (GnomeUIInfo *)main_menu,
				   &myapp);

  { /* build app contents */
    vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

    { /* build vbox contents */
      myapp.toolbar = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
      gtk_container_add(GTK_CONTAINER(vbox), myapp.toolbar);

      w = gtk_check_button_new_with_label("Show grid");
      gtk_container_add(GTK_CONTAINER(myapp.toolbar), w);
      gtk_signal_connect(GTK_OBJECT(w), "toggled", GTK_SIGNAL_FUNC(appwin_toggle_grids), &myapp);

      w = gtk_check_button_new_with_label("Show guides");
      gtk_container_add(GTK_CONTAINER(myapp.toolbar), w);
      gtk_signal_connect(GTK_OBJECT(w), "toggled", GTK_SIGNAL_FUNC(appwin_toggle_guides), &myapp);

      gtk_widget_push_visual(gdk_imlib_get_visual());
      gtk_widget_push_colormap(gdk_imlib_get_colormap());

      table = gtk_table_new(2, 2, FALSE);
      gtk_table_set_row_spacings (GTK_TABLE (table), 4);
      gtk_table_set_col_spacings (GTK_TABLE (table), 4);
      gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

      myapp.canvas = gnome_canvas_new();

      gtk_widget_pop_visual();
      gtk_widget_pop_colormap();

      gnome_canvas_set_pixels_per_unit(GNOME_CANVAS(myapp.canvas), 72);
      gnome_canvas_set_scroll_region(GNOME_CANVAS(myapp.canvas),
				     0.0, 0.0,
				     10.0, 12.0);
      gnome_canvas_scroll_to(GNOME_CANVAS(myapp.canvas), 0, 0);
      gtk_widget_set_usize(myapp.canvas, 500, 600);
      gtk_table_attach (GTK_TABLE (table), myapp.canvas,
			0, 1, 0, 1,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			0, 0);

      w = gtk_hscrollbar_new (GTK_LAYOUT (myapp.canvas)->hadjustment);
      gtk_table_attach (GTK_TABLE (table), w,
			0, 1, 1, 2,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			GTK_FILL,
			0, 0);
      
      w = gtk_vscrollbar_new (GTK_LAYOUT (myapp.canvas)->vadjustment);
      gtk_table_attach (GTK_TABLE (table), w,
			1, 2, 0, 1,
			GTK_FILL,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			0, 0);
    }
    gtk_widget_show_all(vbox);
  }
  gnome_app_set_contents(GNOME_APP(myapp.app), vbox);
  /**
   * end main app window creation
   **/

  gtk_widget_show_all(myapp.app);

  gtk_main();

  return 0;
}
