#ifndef TESTBED_H
#define TESTBED_H 1

#include <gnome.h>
#include "pageitem.h"

typedef struct {
  GtkWidget *app, *canvas, *toolbar;
  GnomeCanvasItem *page;
} TestbedApp;

/* appwin-events.c */
gboolean appwin_delete_event(GtkWidget *widget,
			     GdkEventAny *event,
			     TestbedApp *appinfo);
gboolean appwin_menu_file_quit(GtkWidget *widget, TestbedApp *appinfo);
void appwin_menu_file_new(GtkWidget *widget, TestbedApp *appinfo);
void appwin_toggle_grids(GtkWidget *widget, TestbedApp *appinfo);
void appwin_toggle_guides(GtkWidget *widget, TestbedApp *appinfo);
#endif
