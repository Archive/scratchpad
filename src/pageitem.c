/* By Elliot Lee */
/* TODO: Allow processing of canvas pixel coordinates
         to make them "snap" to guides.

	 Allow allow turning of snapping on/off.
*/
#include "pageitem.h"

#include <math.h>

enum {
	ARG_0,
	ARG_X,
	ARG_Y,
	ARG_WIDTH,
	ARG_HEIGHT,
	ARG_FILL_COLOR,
	ARG_FILL_COLOR_GDK,
	ARG_OUTLINE_COLOR,
	ARG_OUTLINE_COLOR_GDK,
	ARG_GUIDE_COLOR,
	ARG_GUIDE_COLOR_GDK,
	ARG_HMARGIN,
	ARG_VMARGIN,
	ARG_SNAP_TO_GUIDES,
	ARG_DISPLAY_GUIDES,
	ARG_DISPLAY_GRID_H,
	ARG_DISPLAY_GRID_V,
	ARG_GRID_STARTX,
	ARG_GRID_STARTY,
	ARG_GRID_INTERVALX,
	ARG_GRID_INTERVALY,
	ARG_SNAP_TO_GRID,

	ARG_SNAP_THRESHHOLD
};

typedef struct {
  GtkOrientation orient;
  gdouble x, y; /* Coordinates of top left point on the guide line */
  gdouble length;
} PageItemGuideLine;

static void page_item_init(PageItem *item);
static void page_item_class_init(PageItemClass *class);

/* signal handlers */
static void page_item_set_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void page_item_get_arg(GtkObject *object, GtkArg *arg, guint arg_id);
static void page_item_reconfigure(GnomeCanvasItem *item);
static void page_item_realize(GnomeCanvasItem *item);
static void page_item_unrealize(GnomeCanvasItem *item);
static void page_item_translate(GnomeCanvasItem *item, double dx, double dy);
static void page_item_bounds(GnomeCanvasItem *item,
			     double *x1, double *y1,
			     double *x2, double *y2);
static double page_item_point(GnomeCanvasItem *item,
			      double x, double y,
			      int cx, int cy,
			      GnomeCanvasItem **actual_item);

static void page_item_draw(GnomeCanvasItem *item,
			   GdkDrawable *drawable,
			   int x, int y,
			   int width, int height);
static void page_item_draw_margins_and_guides(GnomeCanvasItem *item,
					      GdkDrawable *drawable,
					      int x, int y,
					      gdouble dx, gdouble dy);
static void page_item_draw_grid(GnomeCanvasItem *item,
				GdkDrawable *drawable,
				int x, int y,
				gdouble dx, gdouble dy);

/* utility functions */
static void page_item_set_gc_foreground(GdkGC *gc, gulong pixel);
static void page_item_recalc_bounds(PageItem *pi);

GtkType
page_item_get_type(void)
{
  static GtkType page_item_type = 0;
  static GtkTypeInfo page_item_info = {
    "PageItem",
    sizeof (PageItem),
    sizeof (PageItem),
    (GtkClassInitFunc) page_item_class_init,
    (GtkObjectInitFunc) page_item_init,
    NULL, /* reserved_1 */
    NULL, /* reserved_2 */
    (GtkClassInitFunc) NULL
  };
  
  if(!page_item_type)
    page_item_type = gtk_type_unique(gnome_canvas_item_get_type(),
				     &page_item_info);

  return page_item_type;
}

static void
page_item_class_init(PageItemClass *class)
{
  GnomeCanvasItemClass *item_class;
  GtkObjectClass *object_class;

  object_class = GTK_OBJECT_CLASS(class);
  item_class = GNOME_CANVAS_ITEM_CLASS(class);

  gtk_object_add_arg_type ("PageItem::x", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("PageItem::y", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("PageItem::width", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_WIDTH);
  gtk_object_add_arg_type ("PageItem::height", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_HEIGHT);
  gtk_object_add_arg_type ("PageItem::fill_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_FILL_COLOR);
  gtk_object_add_arg_type ("PageItem::fill_color_gdk", GTK_TYPE_GDK_COLOR, GTK_ARG_READWRITE, ARG_FILL_COLOR_GDK);
  gtk_object_add_arg_type ("PageItem::outline_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_OUTLINE_COLOR);
  gtk_object_add_arg_type ("PageItem::outline_color_gdk", GTK_TYPE_GDK_COLOR, GTK_ARG_READWRITE, ARG_OUTLINE_COLOR_GDK);
  gtk_object_add_arg_type ("PageItem::guide_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_GUIDE_COLOR);
  gtk_object_add_arg_type ("PageItem::guide_color_gdk", GTK_TYPE_GDK_COLOR, GTK_ARG_READWRITE, ARG_GUIDE_COLOR_GDK);

  gtk_object_add_arg_type ("PageItem::snap_threshhold", GTK_TYPE_LONG, GTK_ARG_READWRITE, ARG_SNAP_THRESHHOLD);
  gtk_object_add_arg_type ("PageItem::snap_to_guides", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_SNAP_TO_GUIDES);
  gtk_object_add_arg_type ("PageItem::display_guides", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_DISPLAY_GUIDES);

  gtk_object_add_arg_type ("PageItem::horizontal_margins", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_HMARGIN);
  gtk_object_add_arg_type ("PageItem::vertical_margins", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_VMARGIN);

  gtk_object_add_arg_type ("PageItem::display_grid_horizontal", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_DISPLAY_GRID_H);
  gtk_object_add_arg_type ("PageItem::display_grid_vertical", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_DISPLAY_GRID_V);
  gtk_object_add_arg_type ("PageItem::grid_start_x", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_GRID_STARTX);
  gtk_object_add_arg_type ("PageItem::grid_start_y", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_GRID_STARTY);
  gtk_object_add_arg_type ("PageItem::gridline_spacing_x", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_GRID_INTERVALX);
  gtk_object_add_arg_type ("PageItem::gridline_spacing_y", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_GRID_INTERVALY);
  gtk_object_add_arg_type ("PageItem::snap_to_grid", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_SNAP_TO_GRID);

  object_class->set_arg = page_item_set_arg;
  object_class->get_arg = page_item_get_arg;

  item_class->reconfigure = page_item_reconfigure;
  item_class->realize = page_item_realize;
  item_class->unrealize = page_item_unrealize;
  item_class->translate = page_item_translate;
  item_class->bounds = page_item_bounds;
  item_class->draw = page_item_draw;
  item_class->point = page_item_point;

  class->parent_class = gtk_type_class(gnome_canvas_item_get_type());
}

static void
page_item_init(PageItem *item)
{
  GdkColor color = {0,49152,49152,65535};

  item->hmargin = 1.0;
  item->vmargin = 1.0;
  item->snapping_set = TRUE;
  item->display_guides = TRUE;
  item->snap_threshhold = 5;

  gdk_imlib_best_color_get(&color);
  item->guide_pixel = color.pixel;

  gdk_color_white(gdk_imlib_get_colormap(), &color);
  item->fill_pixel = color.pixel;

  gdk_color_black(gdk_imlib_get_colormap(), &color);
  item->outline_pixel = color.pixel;
}

static void
page_item_set_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  PageItem *pi;
  GdkColor color;

  item = GNOME_CANVAS_ITEM(object);
  pi = PAGE_ITEM(object);

  switch(arg_id) {
  case ARG_X:
    pi->x = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_Y:
    pi->y = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_WIDTH:
    pi->width = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_HEIGHT:
    pi->height = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_FILL_COLOR:
    if (gnome_canvas_get_color (item->canvas, GTK_VALUE_STRING (*arg), &color)) {
      pi->fill_pixel = color.pixel;
      page_item_set_gc_foreground (pi->fill_gc, pi->fill_pixel);
    }
    break;

  case ARG_FILL_COLOR_GDK:
    pi->fill_pixel = ((GdkColor *) GTK_VALUE_BOXED (*arg))->pixel;
    page_item_set_gc_foreground (pi->fill_gc, pi->fill_pixel);
    break;

  case ARG_OUTLINE_COLOR:
    if (gnome_canvas_get_color (item->canvas, GTK_VALUE_STRING (*arg), &color)) {
      pi->outline_pixel = color.pixel;
      page_item_set_gc_foreground(pi->outline_gc, pi->outline_pixel);
    }
    break;

  case ARG_OUTLINE_COLOR_GDK:
    pi->outline_pixel = ((GdkColor *) GTK_VALUE_BOXED (*arg))->pixel;
    page_item_set_gc_foreground (pi->outline_gc, pi->outline_pixel);
    break;

  case ARG_GUIDE_COLOR:
    if (gnome_canvas_get_color (item->canvas, GTK_VALUE_STRING (*arg), &color)) {
      pi->guide_pixel = color.pixel;
      page_item_set_gc_foreground (pi->guide_gc, pi->guide_pixel);
    }
    break;

  case ARG_GUIDE_COLOR_GDK:
    pi->guide_pixel = ((GdkColor *) GTK_VALUE_BOXED (*arg))->pixel;
    page_item_set_gc_foreground (pi->guide_gc, pi->guide_pixel);
    break;

  case ARG_HMARGIN:
    pi->hmargin = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_VMARGIN:
    pi->vmargin = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_SNAP_TO_GUIDES:
    pi->snapping_set = GTK_VALUE_BOOL(*arg);
    break;

  case ARG_SNAP_TO_GRID:
    pi->snap_to_grid_set = GTK_VALUE_BOOL(*arg);
    break;

  case ARG_DISPLAY_GUIDES:
    pi->display_guides = GTK_VALUE_BOOL(*arg);
    break;

  case ARG_SNAP_THRESHHOLD:
    pi->snap_threshhold = GTK_VALUE_LONG(*arg);
    break;

  case ARG_DISPLAY_GRID_H:
    pi->display_grid_h = GTK_VALUE_BOOL(*arg);
    break;

  case ARG_DISPLAY_GRID_V:
    pi->display_grid_v = GTK_VALUE_BOOL(*arg);
    break;

  case ARG_GRID_STARTX:
    pi->grid_startx = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_GRID_STARTY:
    pi->grid_starty = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_GRID_INTERVALX:
    pi->grid_intervalx = GTK_VALUE_DOUBLE(*arg);
    break;

  case ARG_GRID_INTERVALY:
    pi->grid_intervaly = GTK_VALUE_DOUBLE(*arg);
    break;

  default:
    g_warning("Unknown argument %d", arg_id);
    break;
  }
}

static void
page_item_get_arg(GtkObject *object, GtkArg *arg, guint arg_id)
{
  PageItem *pi;

  pi = PAGE_ITEM(object);

  switch(arg_id) {
  case ARG_X:
    GTK_VALUE_DOUBLE (*arg) = pi->x;
    break;

  case ARG_Y:
    GTK_VALUE_DOUBLE (*arg) = pi->y;
    break;

  case ARG_WIDTH:
    GTK_VALUE_DOUBLE (*arg) = pi->width;
    break;

  case ARG_HEIGHT:
    GTK_VALUE_DOUBLE (*arg) = pi->height;
    break;

  case ARG_HMARGIN:
    GTK_VALUE_DOUBLE(*arg) = pi->hmargin;
    break;

  case ARG_VMARGIN:
    GTK_VALUE_DOUBLE(*arg) = pi->vmargin;
    break;

  case ARG_SNAP_TO_GUIDES:
    GTK_VALUE_BOOL(*arg) = (gboolean)pi->snapping_set;
    break;

  case ARG_SNAP_TO_GRID:
    GTK_VALUE_BOOL(*arg) = (gboolean)pi->snap_to_grid_set;
    break;

  case ARG_DISPLAY_GUIDES:
    GTK_VALUE_BOOL(*arg) = (gboolean)pi->display_guides;
    break;

  case ARG_SNAP_THRESHHOLD:
    GTK_VALUE_LONG(*arg) = pi->snap_threshhold;
    break;

  case ARG_DISPLAY_GRID_H:
    GTK_VALUE_BOOL(*arg) = pi->display_grid_h;
    break;

  case ARG_DISPLAY_GRID_V:
    GTK_VALUE_BOOL(*arg) = pi->display_grid_v;
    break;

  case ARG_GRID_STARTX:
    GTK_VALUE_DOUBLE(*arg) = pi->grid_startx;
    break;

  case ARG_GRID_STARTY:
    GTK_VALUE_DOUBLE(*arg) = pi->grid_starty;
    break;

  case ARG_GRID_INTERVALX:
    GTK_VALUE_DOUBLE(*arg) = pi->grid_intervalx;
    break;

  case ARG_GRID_INTERVALY:
    GTK_VALUE_DOUBLE(*arg) = pi->grid_intervaly;
    break;

  case ARG_FILL_COLOR_GDK:
  case ARG_OUTLINE_COLOR_GDK:
  case ARG_GUIDE_COLOR_GDK:
    {
      GdkColor *color = g_new(GdkColor, 1);
      switch(arg_id) {
      case ARG_FILL_COLOR_GDK:
	color->pixel = pi->fill_pixel;
	break;
      case ARG_OUTLINE_COLOR_GDK:
	color->pixel = pi->outline_pixel;
	break;
      case ARG_GUIDE_COLOR_GDK:
	color->pixel = pi->guide_pixel;
	break;
      }

      gdk_color_context_query_color (GNOME_CANVAS_ITEM (pi)->canvas->cc, color);
      GTK_VALUE_BOXED (*arg) = color;
    }
    break;

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void page_item_reconfigure(GnomeCanvasItem *item)
{
  PageItem *pi = PAGE_ITEM(item);

  if (PAGE_ITEM_CLASS(GTK_OBJECT(pi)->klass)->parent_class->reconfigure)
    (* PAGE_ITEM_CLASS(GTK_OBJECT(pi)->klass)->parent_class->reconfigure) (item);

  page_item_set_gc_foreground(pi->fill_gc, pi->fill_pixel);
  page_item_set_gc_foreground(pi->outline_gc, pi->outline_pixel);

  page_item_recalc_bounds(pi);
}

static void
page_item_realize(GnomeCanvasItem *item)
{
  PageItem *pi;
  pi = PAGE_ITEM(item);

  if (PAGE_ITEM_CLASS(GTK_OBJECT(pi)->klass)->parent_class->realize)
    (* PAGE_ITEM_CLASS(GTK_OBJECT(pi)->klass)->parent_class->realize) (item);

  pi->fill_gc = gdk_gc_new (item->canvas->layout.bin_window);
  page_item_set_gc_foreground(pi->fill_gc, pi->fill_pixel);

  pi->outline_gc = gdk_gc_new (item->canvas->layout.bin_window);
  page_item_set_gc_foreground(pi->outline_gc, pi->outline_pixel);

  pi->guide_gc = gdk_gc_new (item->canvas->layout.bin_window);
  page_item_set_gc_foreground(pi->guide_gc, pi->guide_pixel);

  (* GNOME_CANVAS_ITEM_CLASS (item->object.klass)->reconfigure) (item);
}

static void
page_item_unrealize(GnomeCanvasItem *item)
{
  PageItem *pi = PAGE_ITEM(item);

  gdk_gc_unref (pi->fill_gc);
  gdk_gc_unref (pi->outline_gc);
  
  if (PAGE_ITEM_CLASS(GTK_OBJECT(pi)->klass)->parent_class->unrealize)
    (* PAGE_ITEM_CLASS(GTK_OBJECT(pi)->klass)->parent_class->unrealize) (item);
}

static void
page_item_translate(GnomeCanvasItem *item, double dx, double dy)
{
  PageItem *pi = PAGE_ITEM(item);

  pi->x += dx;
  pi->y += dy;

  page_item_recalc_bounds (pi);
}

static void
page_item_bounds(GnomeCanvasItem *item,
		 double *x1, double *y1,
		 double *x2, double *y2)
{
  PageItem *pi;

  pi = PAGE_ITEM (item);

  *x1 = pi->x; *y1 = pi->y;
  *x2 = pi->x + pi->width; *y2 = pi->y + pi->height;
}

static void
page_item_draw_margins_and_guides(GnomeCanvasItem *item,
				  GdkDrawable *drawable,
				  int x, int y,
				  gdouble dx, gdouble dy)
{
  GSList *list_iter;
  PageItemGuideLine *curguide;
  int drawx, drawy, drawwidth, drawheight, drawx2, drawy2;
  PageItem *pi;

  pi = PAGE_ITEM(item);

  /* draw margin guides */
  drawx = drawy = drawwidth = drawheight = 0;
  gnome_canvas_w2c (item->canvas,
		    dx + pi->x + pi->hmargin,
		    dy + pi->y + pi->vmargin, &drawx, &drawy);
  gnome_canvas_w2c (item->canvas,
		    dx + pi->x + pi->width - pi->hmargin,
		    dy + pi->y + pi->height - pi->vmargin,
		    &drawwidth, &drawheight);
  drawwidth -= drawx;
  drawheight -= drawy;
  gdk_draw_rectangle (drawable,
		      pi->guide_gc,
		      FALSE,
		      drawx - x,
		      drawy - y,
		      drawwidth,
		      drawheight);

  /* draw guides */
  for(list_iter = pi->guides; list_iter; list_iter = g_slist_next(list_iter)) {
    curguide = list_iter->data;

    gnome_canvas_w2c(item->canvas,
		     dx + pi->x + curguide->x,
		     dy + pi->y + curguide->y,
		     &drawx, &drawy);

    if(curguide->orient == GTK_ORIENTATION_HORIZONTAL) {
      gnome_canvas_w2c(item->canvas,
		       dx + pi->x + curguide->x + curguide->length,
		       dy + pi->y + curguide->y, &drawx2, &drawy2);
    } else { /* GTK_ORIENTATION_VERTICAL */
      gnome_canvas_w2c(item->canvas,
		       dx + pi->x + curguide->x,
		       dy + pi->y + curguide->y + curguide->length,
		       &drawx2, &drawy2);
    }

    gdk_draw_line(drawable, pi->guide_gc, drawx - x, drawy - y,
		  drawx2 - x, drawy2 - y);
  }
}

static void
page_item_draw_grid(GnomeCanvasItem *item,
		    GdkDrawable *drawable,
		    int x, int y,
		    gdouble dx, gdouble dy)
{
  /* XXX the meanings of "horizontal" and "vertical" WRT guides
     and the grid are confusing - e.g. do we mean
     "vertical lines that are drawn in a horizontal stack" (like at present) or
     "horizontal lines that are drawn in a vertical stack" ? */
  PageItem *pi = PAGE_ITEM(item);
  gdouble curx, cury;
  gint drawx1, drawy1, drawx2, drawy2;

  if(pi->display_grid_h
     && pi->grid_starty < pi->height
     && pi->grid_intervaly != 0.0) {
    for(cury = pi->grid_starty + pi->y;
	cury < (pi->y + pi->height);
	cury += pi->grid_intervaly) {

      gnome_canvas_w2c(item->canvas, dx + pi->x + pi->grid_startx,
		       dy + cury, &drawx1, &drawy1);

      gnome_canvas_w2c(item->canvas, dx + pi->x + pi->width,
		       dy + cury, &drawx2, &drawy2);

      gdk_draw_line(drawable, pi->guide_gc,
		    drawx1 - x, drawy1 - y,
		    drawx2 - x, drawy2 - y);
    }
  }

  if(pi->display_grid_v
     && pi->grid_startx < pi->width
     && pi->grid_intervaly != 0.0) {
    for(curx = pi->grid_startx + pi->x;
	curx < (pi->x + pi->width);
	curx += pi->grid_intervalx) {

      gnome_canvas_w2c(item->canvas, dx + curx,
		       dy + pi->y + pi->grid_starty,
		       &drawx1, &drawy1);

      gnome_canvas_w2c(item->canvas, dx+curx,
		       dy + pi->y + pi->height,
		       &drawx2, &drawy2);

      gdk_draw_line(drawable, pi->guide_gc,
		    drawx1 - x, drawy1 - y,
		    drawx2 - x, drawy2 - y);
    }
  }
}

static void
page_item_draw(GnomeCanvasItem *item,
	       GdkDrawable *drawable,
	       int x, int y,
	       int width, int height)
{
  PageItem *pi;
  double dx, dy;
  int drawx, drawy, drawwidth, drawheight;

  pi = PAGE_ITEM (item);

  /* Get canvas pixel coordinates */

  dx = 0.0;
  dy = 0.0;

  gnome_canvas_item_i2w (item, &dx, &dy);

  drawx = drawy = drawwidth = drawheight = 0;
  gnome_canvas_w2c (item->canvas, dx + pi->x, dy + pi->y, &drawx, &drawy);
  gnome_canvas_w2c (item->canvas,
		    dx + pi->x + pi->width,
		    dy + pi->y + pi->height,
		    &drawwidth, &drawheight);
  drawwidth -= drawx;
  drawheight -= drawy;

  gdk_draw_rectangle (drawable,
		      pi->fill_gc,
		      TRUE,
		      drawx - x,
		      drawy - y,
		      drawwidth,
		      drawheight);

  if(pi->display_guides)
    page_item_draw_margins_and_guides(item, drawable, x, y, dx, dy);

  if(pi->display_grid_h || pi->display_grid_v)
    page_item_draw_grid(item, drawable, x, y, dx, dy);

  /* draw page border */
  gdk_draw_rectangle (drawable,
		      pi->outline_gc,
		      FALSE,
		      drawx - x,
		      drawy - y,
		      drawwidth - 1,
		      drawheight - 1);
}

static double
page_item_point(GnomeCanvasItem *item,
		double x, double y,
		int cx, int cy,
		GnomeCanvasItem **actual_item)
{
  PageItem *pi;
  double x1, y1, x2, y2;
  double dx, dy;

  pi = PAGE_ITEM (item);

  *actual_item = item;

	/* Find the bounds for the rectangle plus its outline width */

  x1 = pi->x;
  y1 = pi->y;
  x2 = pi->x + pi->width - 1;
  y2 = pi->y + pi->width - 1;

  if ((x >= x1) && (y >= y1) && (x <= x2) && (y <= y2))
    /* Yes, point is inside rectangle */
    return 0.0;

  /* Point is outside rectangle */

  if (x < x1)
    dx = x1 - x;
  else if (x > x2)
    dx = x - x2;
  else
    dx = 0.0;

  if (y < y1)
    dy = y1 - y;
  else if (y > y2)
    dy = y - y2;
  else
    dy = 0.0;

  return sqrt (dx * dx + dy * dy);
}

static void
page_item_set_gc_foreground(GdkGC *gc, gulong pixel)
{
  GdkColor c;

  if (!gc)
    return;

  c.pixel = pixel;
  gdk_gc_set_foreground (gc, &c);
}

static void
page_item_recalc_bounds (PageItem *pi)
{
  GnomeCanvasItem *item;
  double x1, y1, x2, y2;

  item = GNOME_CANVAS_ITEM (pi);

  x1 = pi->x;
  y1 = pi->y;
  x2 = pi->x + pi->width;
  y2 = pi->y + pi->height;

  gnome_canvas_item_i2w (item, &x1, &y1);
  gnome_canvas_item_i2w (item, &x2, &y2);
  gnome_canvas_w2c (item->canvas, x1, y1, &item->x1, &item->y1);
  gnome_canvas_w2c (item->canvas, x2, y2, &item->x2, &item->y2);
  
  /* Some safety fudging */

  item->x1 --;
  item->y1 --;
  item->x2 ++;
  item->y2 ++;

  gnome_canvas_group_child_bounds (GNOME_CANVAS_GROUP (item->parent), item);
}

/* Public methods */
GtkWidget *
page_item_new(void)
{
  return gtk_type_new(page_item_get_type());
}

void
page_item_add_guide(PageItem *page_item,
		    GtkOrientation orientation,
		    gdouble x, gdouble y,
		    gdouble length)
{
  PageItemGuideLine *newline;

  g_return_if_fail(IS_PAGE_ITEM(page_item));

  newline = g_new(PageItemGuideLine, 1);
  newline->orient = orientation;
  newline->x = x; newline->y = y;
  newline->length = length;

  page_item->guides = g_slist_prepend(page_item->guides, newline);
}

/* snapping... */
static
void page_item_snap_item_to_grid(PageItem *page_item,
				 gulong *x1, gulong *y1,
				 gulong *x2, gulong *y2)
{
}				 

void page_item_snap_item(PageItem *page_item,
			 gulong x1, gulong y1, gulong x2, gulong y2,
			 glong *dx, glong *dy)
{
  gint lowest_distance, dist;
  glong tmp_dx, tmp_dy;
  gulong tx1, ty1, tx2, ty2;

  tx1 = x1; ty1 = y1; tx2 = x2; ty2 = y2;

  page_item_snap_item_to_grid(page_item, &tx1, &ty1, &tx2, &ty2);

  *dx = tx1 - x1;
  *dy = ty1 - y1;
}
